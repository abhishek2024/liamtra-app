
import { NgModule } from '@angular/core';
import { HomeComponent } from './home/home.component';
import { ViderSliderComponent } from './video-slider/video-slider.component';
import { SharedModule } from '../components/shared/shared.module';
import { CoreRouting } from './core.routing';
import { CoreService } from './shared';
import { UserVerificationComponent } from './user-verification/user-verification.component';



@NgModule({
  declarations: [
    ViderSliderComponent,
    HomeComponent,
    UserVerificationComponent
  ],
  imports: [
    SharedModule,
    CoreRouting
  ],
  providers: [
    CoreService
  ],
  exports: [
    SharedModule,
  ]
})
export class CoreModule { }
