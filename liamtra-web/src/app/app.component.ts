import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: '<app-message></app-message><angular-loader></angular-loader><router-outlet></router-outlet>',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = 'app';
}

