
import { Component, OnInit, Input, Output, EventEmitter, OnChanges, ViewContainerRef } from '@angular/core';
import { HostService, HostModel, HostNewModal } from '../../../components/host/shared';
import { CountryModel } from '../country.model';
import { CityModel } from '../city.model';
import { StateModel } from '../state.model';
import { LoaderService } from '../../../core/loader/loader.service';
import { MessageService } from '../../../shared/message/messageService.service';

@Component({
  selector: 'app-location',
  templateUrl: './location.component.html'
})
export class LocationComponent implements OnInit, OnChanges {
  @Input() hostModel: HostModel;
  @Input() hostNewModel: HostNewModal;
  @Input() mapSettingName: string = '';

  countryId: Number = 0;
  stateId: Number = 0;


  public cities: Array<CityModel> = new Array<CityModel>();
  public countries: Array<CountryModel> = new Array<CountryModel>();
  public states: Array<StateModel> = new Array<StateModel>();
  @Output() onSearchElement = new EventEmitter();
  @Output() onSetCordinates = new EventEmitter();


  constructor(private hostService: HostService,
    public messageService: MessageService,
    private loaderService: LoaderService
  ) {


  }

  ngOnInit() {
    this.getCountries();
    if (this.hostNewModel.stateId > 0 && this.hostNewModel.id > 0) {
      this.countryId = this.hostNewModel.countryId;
      this.getStates(this.hostNewModel.countryId);
      this.getCities(this.hostNewModel.stateId);
      this.stateId = this.hostNewModel.stateId;
    }
  }

  ngOnChanges() {
    if (this.hostModel.countryId > 0) {
      this.getStates(this.hostModel.countryId);
    }

  }

  async  getCountries() {
    try {
      this.loaderService.show();
      const response = await this.hostService.getCountries();
      this.countries = response.data.Result;
      this.loaderService.hide();
    } catch (e) {
      this.loaderService.hide();
    }
  }

  async getStates(countryId) {
    try {
      this.loaderService.show();
      const response = await this.hostService.getStates(countryId);
      this.states = response.data.Result;
      if (this.hostModel.cityId > 0) {
        this.getCities(this.hostModel.stateId);
      }
      this.loaderService.hide();
      this.searchMapEvent();
    } catch (e) {
      this.loaderService.hide();
    }
  }

  async getCities(stateId) {
    try {
      this.loaderService.show();
      const response = await this.hostService.getCities(stateId);
      this.cities = response.data.Result;
      this.loaderService.hide();
      this.searchMapEvent();
    } catch (e) {
      this.loaderService.hide();
    }
  }

  public selectCountry(selectedCountryId: number) {
    this.getStates(selectedCountryId);
  }

  searchMapEvent() {
    if (this.hostModel) {
      let searchItem = '';
      const countryItem = this.countries.find(i => i.countryId === this.hostModel.countryId);
      const stateItem = this.states.find(i => i.stateId === this.hostModel.stateId);
      const cityItem = this.cities.find(i => i.cityId === this.hostModel.cityId);
      if (countryItem) {
        searchItem = countryItem.countryName;
      }
      if (stateItem) {
        searchItem += ',' + stateItem.stateName;
      }
      if (cityItem) {
        searchItem += ',' + cityItem.cityName;
      }
      this.onSearchElement.emit(searchItem);
      this.mapSettingName = searchItem;
    }


    if (this.hostNewModel) {
      let searchItem = '';
      const countryItem = this.countries.find(i => i.countryId === this.countryId);
      const stateItem = this.states.find(i => i.stateId === this.stateId);
      const cityItem = this.cities.find(i => i.cityId === this.hostNewModel.cityId);
      if (countryItem) {
        searchItem = countryItem.countryName;
      }
      if (stateItem) {
        searchItem += ',' + stateItem.stateName;
      }
      if (cityItem) {
        searchItem += ',' + cityItem.cityName;
      }
      this.onSearchElement.emit(searchItem);
      this.mapSettingName = searchItem;
    }
  }

  public selectState(selectedStateId: number) {
    this.getCities(selectedStateId);
  }

  checkValidation(type) {
    let errorCount = 0;
    if (this.hostModel) {
      if (!this.hostModel.cityId || !this.hostModel.address1
        || !this.hostModel.address2 || !this.hostModel.zipCode
        || !this.hostModel.latitude || !this.hostModel.longitude) {
        errorCount++;
      }
    }
    if (this.hostNewModel) {
      if (!this.hostNewModel.cityId || !this.hostNewModel.address1
        || !this.hostNewModel.address2 || !this.hostNewModel.zipCode
        || !this.hostNewModel.latitude || !this.hostNewModel.longitude) {
        errorCount++;
      }
    }

    if (errorCount > 0) {
      if (type === 'all') {
        this.messageService.showMessage({ type: 'warning', title: 'Business Hosting', body: 'Please fill the mandatory fields' });
      }
      return true;
    } else {
      return false;
    }

  }

  searchByCordinate() {
    if (this.hostModel.latitude !== 0 && this.hostModel.longitude !== 0) {
      let obj = {
        cordinateEvent: true,
        latitude: this.hostModel.latitude,
        longitude: this.hostModel.longitude
      }
      this.onSetCordinates.emit(obj);
    } else {
      this.messageService.showMessage({ type: 'warning', title: 'Business Hosting', body: 'Please enter both cordinates for map refresh' });
    }
  }

}
