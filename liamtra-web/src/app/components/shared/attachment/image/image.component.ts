import { Component, OnInit, Input, Output, ViewContainerRef, EventEmitter, OnChanges } from '@angular/core';
import { AttachmentModel } from '../../../host/shared/host.model';
import { AttachmentType } from '../../../../shared/enum/cms-sytem-enum';
import { TourAttachment } from '../../../host/tour';
import { ServiceCategoryEnum } from '../../../../shared/enum/service-category-enum';
import { AssertTypeEnum } from '../../../../shared/enum/cd-key.enum';
import { MessageService } from '../../../../shared/message/messageService.service';
import { ApiUrl } from '../../../../api.service';

@Component({
  selector: 'app-attachment-image',
  templateUrl: './image.component.html'
})

export class AttachmentImageComponent implements OnInit, OnChanges {

  @Input() AttachmentModel: Array<AttachmentModel>;
  @Input() serviceTypeId: number = 0;
  @Input() Attachment: Array<TourAttachment>;
  fileName: String;
  isBusinessDivEnabled: boolean = false;

  constructor(public messageService: MessageService) {
  }

  ngOnInit() { }

  ngOnChanges() {
    console.log(this.Attachment);
    this.Attachment.forEach(file => {
      file.attachmentUrl = ApiUrl.SRC_URI + 'attachments/' + file.fileName;
    });
  }

  onAttachemnt() { }

  async imageUploadEvent(event) {
    const fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      for (let i = 0; i < fileList.length; i++) {
        const type = fileList[i].type.split('/')[1];
        if (type === 'png' || type === 'jpeg' || type === 'jpg' || type === 'gif') {
          const fileToLoad = fileList[i];
          await this.generateFileData(fileToLoad);
        } else {
          this.messageService.showMessage({ type: 'warning', title: 'Business Hosting', body: 'Please use correct format of image' });

        }
      }
    }
  }


  deleteAttachment(index) {
    if (this.serviceTypeId === ServiceCategoryEnum.HostTour
      || this.serviceTypeId === ServiceCategoryEnum.HostBusiness
      || this.serviceTypeId === ServiceCategoryEnum.HostExperience) {
      this.Attachment[index].isDeleted = true;
    } else {
      this.AttachmentModel.splice(index, 1);
    }
  }

  async generateFileData(fileToLoad) {
    const fileReader = new FileReader();
    fileReader.onload = (fileLoadedEvent: any) => {
      const base64Code = fileLoadedEvent.target.result;
      if (this.serviceTypeId === ServiceCategoryEnum.HostTour
        || this.serviceTypeId === ServiceCategoryEnum.HostExperience
        || this.serviceTypeId === ServiceCategoryEnum.HostBusiness) {
        const fileUploadModel = new TourAttachment();
        fileUploadModel.assetTypeId = AssertTypeEnum.Image; /*for image*/
        fileUploadModel.fileName = fileToLoad.name;
        fileUploadModel.attachmentDataBase64 = base64Code;
        this.Attachment.push(fileUploadModel);
      } else {
        this.isBusinessDivEnabled = true;
        const fileUploadModel = new AttachmentModel();
        fileUploadModel.attachmentType = AttachmentType.Image;
        fileUploadModel.fileName = fileToLoad.name;
        fileUploadModel.attachmentDataBase64 = base64Code;
        fileUploadModel.svcTypeId = this.serviceTypeId;
        this.AttachmentModel.push(fileUploadModel);
      }
    };
    fileReader.readAsDataURL(fileToLoad);
  }

}
