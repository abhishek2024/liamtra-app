
import { Http, Headers, RequestOptions, BaseRequestOptions, Response } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { ApiUrl } from '../../../api.service';
import {
  ObjectResponseModel,
  PostObjectResponseModel,
  ArrayResponseModel,
  AsyncArrayPromiseHandler,
  AsyncObjectPromiseHandler,
  BaseDataModel
} from '../../../shared/models/base-data.model';
import { CityModel } from '../../shared/city.model';
import { CustomDDO } from '../custom-ddo.model';
@Injectable()
export class MasterService {

  constructor(private http: Http) { }

  getMonths() {
    const data: any = [{ monthValue: 1, monthName: 'Jan' },
    { monthValue: 2, monthName: 'Feb' },
    { monthValue: 3, monthName: 'Mar' },
    { monthValue: 4, monthName: 'Apr' },
    { monthValue: 5, monthName: 'May' },
    { monthValue: 6, monthName: 'Jun' },
    { monthValue: 7, monthName: 'Jul' },
    { monthValue: 8, monthName: 'Aug' },
    { monthValue: 9, monthName: 'Sep' },
    { monthValue: 10, monthName: 'Oct' },
    { monthValue: 11, monthName: 'Nov' },
    { monthValue: 12, monthName: 'Dec' }];

    const results: Array<CustomDDO> = [];
    data.map((item) => {
      const obj = { label: item.monthName, value: item.monthValue }
      results.push(obj);
    });
    return results;
  }



}
