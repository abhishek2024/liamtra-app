import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { CoreService } from '../../../core/shared';
import { ActivatedRoute, Router } from '@angular/router';
import { LoaderService } from '../../../core/loader/loader.service';
import { UserModal } from '../../../core/login-modal-popup/shared';
import { CommonService } from '../../../shared/services';
import { LoginService } from '../../../core/login-modal-popup/shared';
import { LocalStorageService } from '../../../shared/services';
import { LoginEnum } from '../../../shared/enum/login.enum';
import { MessageService } from '../../../shared/message/messageService.service';
import { MyListingsService } from '../my-listings.service';
@Component({
    selector: 'app-user-profile',
    templateUrl: './user-profile.component.html',
    styleUrls: ['./user-profile.component.css'],
    providers: [CommonService, LocalStorageService, CommonService, CoreService]

})
export class UserProfileComponent implements OnInit {
    tokenLink: string = '';
    SkipBtnText:string='';
    UserDetail = null;
    status: boolean = true;
    gender: Array<any> = [];
    user: UserModal = new UserModal();
    emailDisabled: boolean = false;
    mobilelDisabled: boolean = false;
    isReset: boolean = false;
    emailPattern = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    phonenoPattern = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
    rePassword: string = '';

    constructor(private coreService: CoreService,
        private router: Router,
        private loginService: LoginService,
        private loaderService: LoaderService,
        private localStorageService: LocalStorageService,
        private commonService: CommonService,
        private myListings: MyListingsService,
        private messageService: MessageService,
        public route: ActivatedRoute) {
        this.getGender();

        this.tokenLink = this.route.snapshot.queryParams['token'];
        if (this.tokenLink) {
           this.SkipBtnText="Skip";
            this.user.UserId = this.route.snapshot.queryParams['userId'] || 0;
            this.user.UserEmail = this.route.snapshot.queryParams['email'] || '';
            this.user.UserContactNo = this.route.snapshot.queryParams['mobile'] || '';
            this.status = this.route.snapshot.queryParams['mobileStatus'];
            this.isReset = this.route.snapshot.queryParams['isReset'] === 'True' ? true : false;
            // if (!this.isReset) {
            //     this.getGender();

            // }
            this.emailDisabled = this.user.UserEmail === '' ? false : true;
            this.mobilelDisabled = this.user.UserContactNo && this.user.UserContactNo !== '' ? true : false;
            if (this.emailDisabled) {
                this.emailLinkVerification();
            }
        } else {
            this.SkipBtnText="Return To Home Page";
            let UserDataId = JSON.parse((localStorage.getItem('userId')));
            this.userDetail(UserDataId);
            this.status = true;
        }
    }
    ngOnInit() {
      
       
    }
    async userDetail(UserDataID) {
        this.UserDetail = null;
        try {
            this.loaderService.show();
            const response = await this.myListings.getUserDetail(UserDataID);
            this.UserDetail = response.data.Result;
            this.user.UserId = this.UserDetail.userId;
            this.user.UserContactNo = this.UserDetail.userContactNo;
            this.user.UserFirstName = this.UserDetail.userFirstName;
            this.user.UserMiddleName = this.UserDetail.userMiddleName;
            this.user.UserLastName = this.UserDetail.userLastName;
            this.user.UserEmail = this.UserDetail.userEmail;
            this.user.Gender = this.UserDetail.gender;
            this.loaderService.hide();

        } catch (e) {
            this.loaderService.hide();
        }
    }

    async emailLinkVerification() {
        this.loaderService.show();
        try {
            const response = await this.coreService.emailLinkVerification(this.user.UserEmail, this.tokenLink);
            const result = response.data.Result;
            this.user.UserId = result.userId;
            this.status = true;
            const obj = {
                Password: result.userPwd,
                UserName: result.userEmail,
                provider: LoginEnum.LIAMTRA
            };
            if (!this.isReset) {
                this.login(obj);
            } else {
                this.loaderService.hide();
            }
        } catch (e) {
            this.status = false;
            this.loaderService.hide();
        }
    }

    async login(signIn) {
        try {
            this.loaderService.show();
            const response = await this.loginService.saveSignIn(signIn);
            let token = response.data.Result;
            if (token && token.AccessToken) {
                this.localStorageService.setCurrentUser(token);
            }
            this.loaderService.hide();
            this.commonService.notifyOther({ option: 'onSetHeader', value: true });
        } catch (e) {
            // console.log(e);
            this.loaderService.hide();
        }
    }

    async saveUseretail() {
        if (this.user.UserFirstName.trim() === '' || this.user.UserLastName.trim() === ''
            || this.user.Gender === 0) {


            this.messageService.showMessage({ type: 'warning', title: '', body: 'Please fill the mandatory fields.' });

            return;
        }

        if ((!this.emailPattern.test(this.user.UserEmail))) {
            this.messageService.showMessage({ type: 'warning', title: '', body: 'Please enter valid email-id' });

            return;
        }

        if ((!this.phonenoPattern.test(this.user.UserContactNo))) {
            this.messageService.showMessage({ type: 'warning', title: '', body: 'Please enter valid phone-no.' });
            return;
        }

        this.loaderService.show();
        try {
            const response = await this.coreService.saveUserDetails(this.user);
            this.loaderService.hide();
            this.skipDetail();

        } catch (e) {
            console.log("error")
            this.loaderService.hide();
        }
    }

    async getGender() {
        this.loaderService.show();
        try {
            const response = await this.coreService.getGender();
            this.gender = response.data.Result;
            this.loaderService.hide();
        } catch (e) {
            console.log(e);
            this.loaderService.hide();
        }
    }

    skipDetail() {
        this.router.navigate(['/']);
    }

    async updatePassword() {
        if (this.user.UserPwd.trim() === '') {
            this.messageService.showMessage({ type: 'warning', title: '', body: 'Both fields are mandatory' });

            return;
        }
        if (this.user.UserPwd.trim() !== this.rePassword) {
            this.messageService.showMessage({ type: 'warning', title: '', body: 'Password should be same in both fields' });

            return;
        }
        this.loaderService.show();
        try {
            let obj = {
                UserName: this.user.UserEmail,
                UserPwd: this.user.UserPwd
            }
            const response = await this.loginService.changePassword(obj);
            this.messageService.showMessage({ type: 'warning', title: '', body: 'Password changed successfully.' });

            this.cancel();
            this.loaderService.hide();
        } catch (e) {
            console.log(e);
            this.loaderService.hide();
        }
    }

    cancel() {
        this.router.navigate(['']);
    }
}
