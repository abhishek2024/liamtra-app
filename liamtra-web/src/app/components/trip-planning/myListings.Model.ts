
  export interface ToursElement {
    title: string;
    startDate: string;
    endDate:string;
    source: string;
    destination:string;
    serviceStatusName:string;
    sellerName:string;
    sellerEmail:string;
  }
  
  export interface FavoriteExperiencesListing {
    title: string;
    startDate: string;
    endDate:string;
    source: string;
    destination:string;
    serviceStatusName:string;
    sellerName:string;
    sellerEmail:string;
  }
  export interface FavoriteBusinessListing {
    title: string;
    startDate: string;
    endDate:string;
    source: string;
    destination:string;
    serviceStatusName:string;
    sellerName:string;
    sellerEmail:string;
  }
  export interface FavoriteTourListing {
    tourName: string;
    sellerName: string;
    sellerEmail:string;
    contactNumber: string;
  }
  export interface Element {
    uniqueHome: string;
    listType: string;
    propertyType:string;
    creationDate: string;
    numberOfGuestsInRoom?:number;
  }
  export interface FavoriteUniqueHomeListing {
    uniqueHome: string;
    listType: string;
    propertyType:string;
    creationDate: string;
  }
  
  export interface MyBookedUniqueHomeElement {
    stayName: string;
    hostName: string;
    checkIn:string;
    checkOut:string;
    contactNumber:string
    status:string
  }

  export interface BookedTourElement {
    tourName: string;
    guestName: string;
    emailAddress:string;
    mobileNo:string;
    checkInDate:string
    checkoutDate:string
    statusName: string;
  }

  export interface OwnerUniqueHomeElement {
    hostName: string;
    hostDesc: string;
    emailAddress:string;
    mobileNo:string;
    checkInDate:string
    checkoutDate:string
    statusName: string;
  }
  export interface HostedBusinessElement {
    bussinessName: string;
    emailId:string;
    creationDate: string;
    contactNo:string;
  }
  
  export interface HostedExperienceElement {
    bussinessName: string;
    emailId:string;
    creationDate: string;
    contactNo:string;
  }


