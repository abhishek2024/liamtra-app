import { TestBed, inject } from '@angular/core/testing';

import { MyListingsService } from './my-listings.service';

describe('MyListingsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MyListingsService]
    });
  });

  it('should be created', inject([MyListingsService], (service: MyListingsService) => {
    expect(service).toBeTruthy();
  }));
});
