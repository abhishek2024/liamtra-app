import { Component, OnInit ,ViewChild} from '@angular/core';
import { Router } from '@angular/router';
import {PageEvent} from '@angular/material';
import {MatPaginator, MatTableDataSource} from '@angular/material';
import {MyListingsService} from '../../my-listings.service'
import {BookedTourElement} from '../../myListings.Model'
import { LoaderService } from '../../../../core/loader';
@Component({
  selector: 'app-home-listing',
  templateUrl: './my-tour-booking-listing.component.html',
  styleUrls: ['../../sharedStyle.component.css']
})
export class myTourBokingListingComponent implements OnInit {
  
  displayedColumns = ['Tour Name', 'Guest Name',  'Email Address', 'Mobile No','CheckIn Date', 'Checkout Date','Status','Action'];
  dataSource = new MatTableDataSource<BookedTourElement>(ELEMENT_DATA);
  pageSize = 10;
  showToolbar=false;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  pageEvent: PageEvent;

  constructor(private myListings:MyListingsService,private loaderService: LoaderService) {

  }
 async myBookedCanceledTourListing() {
    try {
      this.loaderService.show();
      let response = await this.myListings.BookedCancelToursList();
      this.dataSource.data = response.data.Result.data;
      this.loaderService.hide();
      if(this.dataSource.data.length<=0){
        this.showToolbar=true;
      }
    } catch (e) {
      this.loaderService.hide();
    }
  }
 
  ngOnInit() {
    this.myBookedCanceledTourListing();
  }
  
    ngAfterViewInit() {
      this.dataSource.paginator = this.paginator;
    }
  }

  const ELEMENT_DATA: BookedTourElement[] = [];