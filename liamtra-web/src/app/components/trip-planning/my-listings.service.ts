import { Injectable } from '@angular/core';
import { HttpModule, Http } from '@angular/http';
import { ApiUrl } from '../../api.service';
import 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import {
  ObjectResponseModel,
  PostObjectResponseModel,
  BaseDataModel,
  ArrayResponseModel,
  AsyncArrayPromiseHandler,
  AsyncObjectPromiseHandler
} from '../../shared/models/base-data.model';
import {
  FavoriteBusinessListing,
  FavoriteExperiencesListing,
  FavoriteTourListing,
  BookedTourElement,
  ToursElement,
  HostedExperienceElement,
  OwnerUniqueHomeElement,
  FavoriteUniqueHomeListing,
  Element, HostedBusinessElement
} from './myListings.Model'
@Injectable()
export class MyListingsService {

  constructor(private http: Http) { }


  uniqueHomeList(): Promise<ObjectResponseModel<BaseDataModel<Element>>> {
    const promise = this.http
      .get(ApiUrl.MASTER_URI + 'HostService/listedUniqueHome');
    return new AsyncObjectPromiseHandler<BaseDataModel<Element>>(promise.toPromise());
  }
  uniqueHomeReportedList(): Promise<ObjectResponseModel<BaseDataModel<Element>>> {
    const promise = this.http
      .get(ApiUrl.MASTER_URI + 'Review/issueReportList');
    return new AsyncObjectPromiseHandler<BaseDataModel<Element>>(promise.toPromise());
  }
  
  myToursList(): Promise<ObjectResponseModel<BaseDataModel<ToursElement>>> {
    const promise = this.http
      .get(ApiUrl.MASTER_URI + 'Tour/getAllMyHostedTour');
    return new AsyncObjectPromiseHandler<BaseDataModel<ToursElement>>(promise.toPromise());
  }


  myReportedToursList(): Promise<ObjectResponseModel<BaseDataModel<any>>> {
    const promise = this.http
      .get(ApiUrl.MASTER_URI + 'Tour/getAllReportedToursByOwner');
    return new AsyncObjectPromiseHandler<BaseDataModel<any>>(promise.toPromise());
  }
  

  BookedCancelToursList(): Promise<ObjectResponseModel<BaseDataModel<any>>> {
    const promise = this.http
      .get(ApiUrl.MASTER_URI + 'TourBooking/getAllBookedToursByOwner');
    return new AsyncObjectPromiseHandler<BaseDataModel<any>>(promise.toPromise());
  }

  MyBookedCancelToursListbyBooker(): Promise<ObjectResponseModel<BaseDataModel<any>>> {
    const promise = this.http
      .get(ApiUrl.MASTER_URI + 'TourBooking/getAllBookedToursByBooker');
    return new AsyncObjectPromiseHandler<BaseDataModel<any>>(promise.toPromise());
  }

  MyBookedCancelToursUpdate(UpdateData): Promise<ArrayResponseModel<any>> {
    const promise = this.http
      .put(ApiUrl.MASTER_URI + 'TourBooking/updateStatus', UpdateData);
    return new AsyncArrayPromiseHandler<any>(promise.toPromise());
  }
  
  MyBookedCancelUniqueHomeUpdate(bookingId): Promise<ArrayResponseModel<any>> {
    const promise = this.http
      .put(ApiUrl.MASTER_URI + 'BookService/updateStatus/' + bookingId, bookingId);
    return new AsyncArrayPromiseHandler<any>(promise.toPromise());
  }
  MyBookedCancelHomeList(): Promise<ObjectResponseModel<BaseDataModel<any>>> {
    const promise = this.http
      .get(ApiUrl.MASTER_URI + 'BookService/listUniqueHomeForBooker');
    return new AsyncObjectPromiseHandler<BaseDataModel<any>>(promise.toPromise());
  }
  OwnerBookedCancelHomeList(): Promise<ObjectResponseModel<BaseDataModel<OwnerUniqueHomeElement>>> {
    const promise = this.http
      .get(ApiUrl.MASTER_URI + 'BookService/listUniqueHomeForOwner');
    return new AsyncObjectPromiseHandler<BaseDataModel<OwnerUniqueHomeElement>>(promise.toPromise());
  }

  GetBusinessHostedListing(): Promise<ObjectResponseModel<BaseDataModel<HostedBusinessElement>>> {
    const promise = this.http
      .get(ApiUrl.MASTER_URI + 'HostBusiness/GetBusiness');
    return new AsyncObjectPromiseHandler<BaseDataModel<HostedBusinessElement>>(promise.toPromise());
  }

  GetBusinessHostedReportedListing(): Promise<ArrayResponseModel<HostedBusinessElement>> {
    const promise = this.http
      .get(ApiUrl.MASTER_URI + 'HostBusiness/Get');
    return new AsyncArrayPromiseHandler<HostedBusinessElement>(promise.toPromise());
  }

  GetExperienceHostedReportedListing(): Promise<ArrayResponseModel<HostedExperienceElement>> {
    const promise = this.http
      .get(ApiUrl.MASTER_URI + 'HostExperience/Get');
    return new AsyncArrayPromiseHandler<HostedExperienceElement>(promise.toPromise());
  }

  GetExperienceHostedListing(): Promise<ObjectResponseModel<BaseDataModel<any>>> {
    const promise = this.http
      .get(ApiUrl.MASTER_URI + 'HostExperience/GetExperience');
    return new AsyncObjectPromiseHandler<BaseDataModel<any>>(promise.toPromise());
  }

  getAllFavoriteHomes(): Promise<ObjectResponseModel<BaseDataModel<FavoriteUniqueHomeListing>>> {
    const promise = this.http
      .get(ApiUrl.MASTER_URI + 'HostService/getAllFavorite');
    return new AsyncObjectPromiseHandler<BaseDataModel<FavoriteUniqueHomeListing>>(promise.toPromise());
  }


  getAllFavoriteTourPackage(): Promise<ArrayResponseModel<FavoriteTourListing>> {
    const promise = this.http
      .get(ApiUrl.MASTER_URI + 'UserServiceFavorite/getAllFavoriteToursByUser');
    return new AsyncArrayPromiseHandler<FavoriteTourListing>(promise.toPromise());
  }

  getAllFavoriteExperiences(): Promise<ArrayResponseModel<FavoriteExperiencesListing>> {
    const promise = this.http
      .get(ApiUrl.MASTER_URI + 'UserServiceFavorite/getAllFavoriteExperienceByUser');
    return new AsyncArrayPromiseHandler<FavoriteExperiencesListing>(promise.toPromise());
  }

  getAllFavoriteBusiness(): Promise<ArrayResponseModel<FavoriteBusinessListing>> {
    const promise = this.http
      .get(ApiUrl.MASTER_URI + 'UserServiceFavorite/getAllFavoriteBusinessByUser');
    return new AsyncArrayPromiseHandler<FavoriteBusinessListing>(promise.toPromise());
  }

  getUserDetail(id): Promise<ObjectResponseModel<BaseDataModel<any>>> {
    const promise = this.http
    .get(ApiUrl.MASTER_URI + 'User/get/'+id);
    return new AsyncObjectPromiseHandler<BaseDataModel<any>>(promise.toPromise());
  }

  
}
