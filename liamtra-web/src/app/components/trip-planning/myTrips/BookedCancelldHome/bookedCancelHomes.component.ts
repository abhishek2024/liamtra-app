import { Component, OnInit ,ViewChild} from '@angular/core';
import { Router } from '@angular/router';
import {PageEvent} from '@angular/material';
import {MatPaginator, MatTableDataSource} from '@angular/material';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {MyDialogComponent} from '../../my-dialog/my-dialog.component';
import {MyListingsService} from '../../my-listings.service'
import {MyBookedUniqueHomeElement} from '../../myListings.Model'
import { LoaderService } from '../../../../core/loader';
import { MessageService } from '../../../../shared/message/messageService.service';
import { DatePipe } from '@angular/common';
@Component({
  selector: 'app-home-listing',
  templateUrl: './bookedCancelHomes.component.html',
  styleUrls: ['../../sharedStyle.component.css'],
  providers: [DatePipe]
})
export class BookedCancelHomesComponent implements OnInit {
  displayedColumns = ['Unique Home', 'Host Name',  'CheckIn Date', 'CheckOut Date', 'Status','Action'];
  dataSource = new MatTableDataSource<MyBookedUniqueHomeElement>(ELEMENT_DATA);
  pageSize = 10;
  showToolbar=false;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  pageEvent: PageEvent;
  constructor(private router: Router,private myListings:MyListingsService, public dialog: MatDialog, private loaderService: LoaderService, private messageService:MessageService,private datePipe: DatePipe) {

  }
  async listUniqueHomeForBooker() {

    try {
      this.loaderService.show();
      let response = await this.myListings.MyBookedCancelHomeList();
      this.dataSource.data = response.data.Result.data;
      console.log( this.dataSource.data)
      this.loaderService.hide();
      if(this.dataSource.data.length<=0){
        this.showToolbar=true;
      }
    } catch (e) {
      this.loaderService.hide();
    }
  }
  viewUniqueHome(city,noOfGuests,chekin,checkout,svcId){
    let chekInDate = this.datePipe.transform(chekin,'dd-MM-yyyy');
    let chekOutDate = this.datePipe.transform(checkout,'dd-MM-yyyy');
    this.router.navigate(['/book/unique-homes-detail'],  { queryParams:{ CityName: city, NumberofGuestsInRoom:noOfGuests,CheckIn:chekInDate,CheckOut:chekOutDate,svcId:svcId } });
   }
  ngOnInit() {
    this.listUniqueHomeForBooker();
    
  }
  openDialog(bookingId): void {

    let dialogRef = this.dialog.open(MyDialogComponent, {
      height: '220px',
  width: '400px',
  data: { headerTitle: "My Unique Home", content: "Do You Want To Cancel Unique Home?" }

    });

    dialogRef.afterClosed().subscribe(result => {
      if(result=="Confirm"){
     this.cancelTour(bookingId)
      }
     
    });
  }
  async cancelTour(bookingId){
      try {
        this.loaderService.show();
        let response = await this.myListings.MyBookedCancelUniqueHomeUpdate(bookingId);
        this.dataSource.data = response.data.Result;
        this.loaderService.hide();
        this.listUniqueHomeForBooker();
        this.messageService.showMessage({ type: 'success', title: '', body: 'Cancelled successfully' });
      } catch (e) {
        this.messageService.showMessage({ type: 'warning', title: '', body: "Something went wrong" });
        this.loaderService.hide();
      }
      
    }
    ngAfterViewInit() {
      this.dataSource.paginator = this.paginator;
    }

  }
 const ELEMENT_DATA: MyBookedUniqueHomeElement[] = [];