import { Component, OnInit ,ViewChild} from '@angular/core';
import { Router } from '@angular/router';
import {PageEvent} from '@angular/material';
import {MatPaginator, MatTableDataSource} from '@angular/material';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {BookedTourElement} from '../../myListings.Model';
import {MyListingsService} from '../../my-listings.service';
import {MyDialogComponent} from '../../my-dialog/my-dialog.component';
import { LoaderService } from '../../../../core/loader';
import { MessageService } from '../../../../shared/message/messageService.service';
@Component({
  selector: 'app-home-listing',
  templateUrl: './bookedCancelTours.component.html',
  styleUrls: ['../../sharedStyle.component.css']
})
export class BookedCancelToursComponent implements OnInit {

  displayedColumns = ['Tour Name', 'Guest Name',  'CheckIn Date', 'Checkout Date','Status','Action'];
  dataSource = new MatTableDataSource<BookedTourElement>(ELEMENT_DATA);
  pageSize = 10;
  showToolbar=false;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  pageEvent: PageEvent;
  constructor(private myListings:MyListingsService,public dialog: MatDialog,private loaderService: LoaderService,private messageService:MessageService) {

  }
  async MyBookedCancelToursListbyBooker() {

    try {
      this.loaderService.show();
      let response = await this.myListings.MyBookedCancelToursListbyBooker();
      this.dataSource.data = response.data.Result.data;
      this.loaderService.hide();
      if(this.dataSource.data.length<=0){
        this.showToolbar=true;
      }
    } catch (e) {
      this.loaderService.hide();
    }
  }

  ngOnInit() {
    this.MyBookedCancelToursListbyBooker();
  }
  
    ngAfterViewInit() {
      this.dataSource.paginator = this.paginator;
    }
  openDialog(id,tourBookingSatusId, CheckIn, Checkout): void {
    let dialogRef = this.dialog.open(MyDialogComponent, {
      height: '220px',
  width: '400px',
  data: { headerTitle: "My Tour", content: "Do You Want To Cancel Tour?" }

    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
      if(result=="Confirm"){
this.cancelTour(id,tourBookingSatusId, CheckIn, Checkout)
      }
     
    });
  }
  async cancelTour(id,tourBookingSatusId, CheckIn, Checkout){
      let UpdateData={
        "id":id,
        "tourBookingSatusId":2,
        "checkInDate":CheckIn,
        "checkoutDate":Checkout
      }
    try {
          this.loaderService.show();
          let response = await this.myListings.MyBookedCancelToursUpdate(UpdateData);
          this.dataSource.data = response.data.Result;
          this.MyBookedCancelToursListbyBooker();
          this.messageService.showMessage({ type: 'success', title: '', body: "Cancelled Successfully" });
  
          this.loaderService.hide();
        } catch (e) {
          this.messageService.showMessage({ type: 'warning', title: '', body: "Something went wrong" });
          this.loaderService.hide();
        }
      }
    
  }
const ELEMENT_DATA: BookedTourElement[] = [ ];