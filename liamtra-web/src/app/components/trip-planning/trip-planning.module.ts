
import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { TripPlanningComponent } from './trip-planning.component';
import { TripPlanningRouting } from './trip-planning.routing';
import { TripPlanningSideBarComponent } from './tripPlaningSidebar/trip-planning-sidebar.component';
import { TripHomeListingComponent } from './myListings/homeListing/home-listing.component';
import { HomeListingReportedComponent } from './myListings/homeListingReported/home-listingReported.component';
import { TripExperienceListingComponent } from './myListings/experienceListing/experience-listing.component';
import { ExperienceListingReportedComponent } from './myListings/experienceListingReported/experience-listingReported.component';
import {BusinessListingComponent} from './myListings/businessListing/business-listing.component';
import {BusinessListingReportedComponent} from './myListings/businessListingReported/business-listingReported.component';
import {BookedCancelHomesComponent} from './myTrips/BookedCancelldHome/bookedCancelHomes.component';
import {BookedCancelToursComponent} from './myTrips/BookedCancelldTours/bookedCancelTours.component';
import {FavoriteUniqueHomeComponent} from './Favorite/favorite-UniqueHome/favorite-uniquehome.component';
import {FavoriteTourPackageComponent} from './Favorite/favorite-TourPackage/favorite-tours.component';
import {FavoriteExperiencesComponent} from './Favorite/favorite-Experiences/favorite-experiences.component';
import {FavoriteBusinessComponent} from './Favorite/favorite-Business/favorite-business.component';
import {myHomeBokingListingComponent} from './myBookingListings/myHomeBookingListings/my-home-booking-listing.component';
import {myTourBokingListingComponent} from './myBookingListings/myTourBookingListings/my-tour-booking-listing.component';
import {ToursListingComponent} from './myListings/toursListing/tours-listing.component';
import {MyListingsService} from './my-listings.service';
import { MyDialogComponent } from './my-dialog/my-dialog.component';
import {ToursReportedListingComponent} from './myListings/toursReportedListing/tours-reported-listing.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
@NgModule({
  declarations: [
    TripPlanningComponent,
    TripPlanningSideBarComponent,
    TripHomeListingComponent,
    HomeListingReportedComponent,
    TripExperienceListingComponent,
    ExperienceListingReportedComponent,
    BusinessListingComponent,
    BusinessListingReportedComponent,
    BookedCancelHomesComponent,
    BookedCancelToursComponent,
    FavoriteUniqueHomeComponent,
    FavoriteTourPackageComponent,
    FavoriteExperiencesComponent,
    FavoriteBusinessComponent,
    myHomeBokingListingComponent,
    myTourBokingListingComponent,
    ToursListingComponent,
    MyDialogComponent,
    ToursReportedListingComponent,
    UserProfileComponent,
  ],
  imports: [
    SharedModule,
    TripPlanningRouting
  ],
  providers:    [ MyListingsService ],
  exports: [
    SharedModule
  ],
  entryComponents: [MyDialogComponent]
})
export class TripPlanningModule { }
