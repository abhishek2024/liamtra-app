import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { TripPlanningComponent } from './trip-planning.component';
import { TripHomeListingComponent } from './myListings/homeListing/home-listing.component';
import { HomeListingReportedComponent } from './myListings/homeListingReported/home-listingReported.component';
import { TripExperienceListingComponent } from './myListings/experienceListing/experience-listing.component';
import { ExperienceListingReportedComponent } from './myListings/experienceListingReported/experience-listingReported.component';
import { BusinessListingComponent } from './myListings/businessListing/business-listing.component';
import { BusinessListingReportedComponent } from './myListings/businessListingReported/business-listingReported.component';
import { BookedCancelHomesComponent } from './myTrips/BookedCancelldHome/bookedCancelHomes.component';
import { BookedCancelToursComponent } from './myTrips/BookedCancelldTours/bookedCancelTours.component';
import { FavoriteUniqueHomeComponent } from './Favorite/favorite-UniqueHome/favorite-uniquehome.component';
import { FavoriteTourPackageComponent } from './Favorite/favorite-TourPackage/favorite-tours.component';
import { FavoriteExperiencesComponent } from './Favorite/favorite-Experiences/favorite-experiences.component';
import { FavoriteBusinessComponent } from './Favorite/favorite-Business/favorite-business.component';
import { myHomeBokingListingComponent } from './myBookingListings/myHomeBookingListings/my-home-booking-listing.component';
import { myTourBokingListingComponent } from './myBookingListings/myTourBookingListings/my-tour-booking-listing.component';
import { ToursListingComponent } from './myListings/toursListing/tours-listing.component';
import { ToursReportedListingComponent } from './myListings/toursReportedListing/tours-reported-listing.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
export const routes: Routes = [
  {
    path: '', component: TripPlanningComponent,
    children: [
      { path: '', component: UserProfileComponent },
      { path: 'homeListings', component: TripHomeListingComponent },
      { path: 'tourListings', component: ToursListingComponent },
      { path: 'tourReportedListings', component: ToursReportedListingComponent },
      { path: 'homeListingsReporting', component: HomeListingReportedComponent },
      { path: 'experienceListings', component: TripExperienceListingComponent },
      { path: 'experienceListingsReporting', component: ExperienceListingReportedComponent },
      { path: 'businessListings', component: BusinessListingComponent },
      { path: 'businessListingsReporting', component: BusinessListingReportedComponent },
      { path: 'bookedCancelledHomes', component: BookedCancelHomesComponent },
      { path: 'bookedCancelledTours', component: BookedCancelToursComponent },
      { path: 'favUniqueHomeListings', component: FavoriteUniqueHomeComponent },
      { path: 'favTourPackages', component: FavoriteTourPackageComponent },
      { path: 'favExperiences', component: FavoriteExperiencesComponent },
      { path: 'favBusiness', component: FavoriteBusinessComponent },
      { path: 'myHomeBookingListings', component: myHomeBokingListingComponent },
      { path: 'myTourBookingListings', component: myTourBokingListingComponent }

    ]
  },

];

export const TripPlanningRouting: ModuleWithProviders = RouterModule.forChild(routes);
