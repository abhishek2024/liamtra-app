import { Component, OnInit ,ViewChild} from '@angular/core';
import { Router } from '@angular/router';
import {PageEvent} from '@angular/material';
import {MatPaginator, MatTableDataSource} from '@angular/material';
import {MyListingsService} from '../../my-listings.service';
import {HostedBusinessElement} from '../../myListings.Model';
import { LoaderService } from '../../../../core/loader';
@Component({
  selector: 'app-home-listing',
  templateUrl: './business-listing.component.html',
  styleUrls: ['../../sharedStyle.component.css']
})
export class BusinessListingComponent implements OnInit {
  displayedColumns = ['Business Name', 'Business Description','Email Id',  'Contact No','Status','Action'];
  dataSource = new MatTableDataSource<HostedBusinessElement>(ELEMENT_DATA);
  pageSize = 10;
  showToolbar=false;
@ViewChild(MatPaginator) paginator: MatPaginator;
  pageEvent: PageEvent;

  constructor(private myListings:MyListingsService,private loaderService: LoaderService,private router: Router) {
   
  }
  async myBusinessListing() {
try {
      this.loaderService.show();
      let response = await this.myListings.GetBusinessHostedListing();
      this.dataSource.data = response.data.Result.data;
      this.loaderService.hide();
      if(this.dataSource.data.length<=0){
        this.showToolbar=true;
      }
    } catch (e) {
      this.loaderService.hide();
    }
  }
  viewBusiness(city,id){
    this.router.navigate(['/book/business-detail'], { queryParams: { CityName: city,id:id } });
   }
ngOnInit() {
  this.myBusinessListing();
}

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }
}
const ELEMENT_DATA: HostedBusinessElement[] = [];