import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { PageEvent } from '@angular/material';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { MyListingsService } from '../../my-listings.service';
import { HostedExperienceElement } from '../../myListings.Model';
import { LoaderService } from '../../../../core/loader';
@Component({
  selector: 'app-home-listing',
  templateUrl: './experience-listing.component.html',
  styleUrls: ['../../sharedStyle.component.css']
})
export class TripExperienceListingComponent implements OnInit {
  displayedColumns = ['Experience Name', 'Experience Description', 'Email Id', 'Contact No','Status','Action'];
  dataSource = new MatTableDataSource<HostedExperienceElement>(ELEMENT_DATA);
  pageSize = 10;
  showToolbar=false;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  pageEvent: PageEvent;
  constructor(private myListings: MyListingsService, private loaderService: LoaderService,private router: Router) {
  }

  async myExperienceListing() {
    try {
      this.loaderService.show();
      let response = await this.myListings.GetExperienceHostedListing();
      this.dataSource.data = response.data.Result.data;
      this.loaderService.hide();
      if(this.dataSource.data.length<=0){
        this.showToolbar=true;
      }
      console.log(response)
    } catch (e) {
      this.loaderService.hide();
    }
  }
  viewExperience(city,id){
    this.router.navigate(['/book/experience-detail'], { queryParams: { CityName: city,id:id } });
   }
  ngOnInit() {
    this.myExperienceListing();
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }
}
const ELEMENT_DATA: HostedExperienceElement[] = [

];