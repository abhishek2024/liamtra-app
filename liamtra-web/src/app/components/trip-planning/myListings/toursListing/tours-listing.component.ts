import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { PageEvent } from '@angular/material';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { MyListingsService } from '../../my-listings.service';
import { ToursElement } from '../../myListings.Model';
import { LoaderService } from '../../../../core/loader';
@Component({
  selector: 'app-tours-listing',
  templateUrl: './tours-listing.component.html',
  styleUrls: ['../../sharedStyle.component.css']
})
export class ToursListingComponent implements OnInit {
  displayedColumns = ['Tour Title', 'Start Date', 'End Date', 'Source', 'Destination', 'Status','Action'];
  tourDataSource = new MatTableDataSource<ToursElement>(ELEMENT_DATA);
  pageSize = 10;
  showToolbar=false;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  pageEvent: PageEvent;
  constructor(private myListings: MyListingsService, private loaderService: LoaderService) {
  }

  async myToursListing() {
    try {
      this.loaderService.show();
      let response = await this.myListings.myToursList();
      this.tourDataSource.data = response.data.Result.data;
      this.loaderService.hide();
      if(this.tourDataSource.data.length<=0){
        this.showToolbar=true;
      }
    } catch (e) {
      this.loaderService.hide();
    }
  }

  ngOnInit() {
    this.myToursListing();
  }

  ngAfterViewInit() {
    this.tourDataSource.paginator = this.paginator;
  }
}

const ELEMENT_DATA: ToursElement[] = [

];