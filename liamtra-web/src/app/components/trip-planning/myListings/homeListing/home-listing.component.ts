import { Component, OnInit ,ViewChild} from '@angular/core';
import { Router } from '@angular/router';
import {PageEvent} from '@angular/material';
import {MatPaginator, MatTableDataSource} from '@angular/material';
import {MyListingsService} from '../../my-listings.service';
import {Element} from '../../myListings.Model';
import { LoaderService } from '../../../../core/loader';
import { DatePipe } from '@angular/common';
@Component({
  selector: 'app-home-listing',
  templateUrl: './home-listing.component.html',
  styleUrls: ['../../sharedStyle.component.css'],
  providers: [DatePipe]
})
export class TripHomeListingComponent implements OnInit  {
  displayedColumns = ['Unique Home', 'List Type',  'Property Type', 'Creation Date','Status','Action'];
  dataSource = new MatTableDataSource<Element>(ELEMENT_DATA);
  pageSize = 10;
  showToolbar=false;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  pageEvent: PageEvent;
  constructor(private myListings:MyListingsService,private loaderService: LoaderService,private datePipe: DatePipe,private router: Router) { 
  }
  async myUniqueHomeListing() {
    try {
      this.loaderService.show();
      let response = await this.myListings.uniqueHomeList();
      this.dataSource.data = response.data.Result.data;
      this.loaderService.hide();
      if(this.dataSource.data.length<=0){
        this.showToolbar=true;
      }
    } catch (e) {
      this.loaderService.hide();
    }
  }
  viewUniqueHome(city,noOfGuests,chekin,checkout,svcId){
    let chekInDate = this.datePipe.transform(chekin,'dd-MM-yyyy');
    let chekOutDate = this.datePipe.transform(checkout,'dd-MM-yyyy');
    this.router.navigate(['/book/unique-homes-detail'], { queryParams: { CityName: city, NumberofGuestsInRoom:noOfGuests,CheckIn:chekInDate,CheckOut:chekOutDate,svcId:svcId } });
   }
ngOnInit() {
  this.myUniqueHomeListing();
}
  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }
}
const ELEMENT_DATA: Element[] = [
 
];