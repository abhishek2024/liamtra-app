import { Component, OnInit ,ViewChild} from '@angular/core';
import { Router } from '@angular/router';
import {PageEvent} from '@angular/material';
import {MatPaginator, MatTableDataSource} from '@angular/material';
import {MyListingsService} from '../../my-listings.service';
import {Element} from '../../myListings.Model';
import { LoaderService } from '../../../../core/loader';
import { DatePipe } from '@angular/common';
@Component({
  selector: 'app-home-listingReported',
  templateUrl: './home-listingReported.component.html',
  styleUrls: ['../../sharedStyle.component.css'],
  providers: [DatePipe]
})
export class HomeListingReportedComponent implements OnInit {

  displayedColumns = ['Unique Home', 'Reported Date',  'Reported Message', 'Action'];
  dataSource = new MatTableDataSource<Element>(ELEMENT_DATA);
  pageSize = 10;
  pageSizeOptions = [5, 10, 25, 100];
  uniqueHomeListins=[];
  showToolbar=false;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  pageEvent: PageEvent;

  setPageSizeOptions(setPageSizeOptionsInput: string) {
    this.pageSizeOptions = setPageSizeOptionsInput.split(',').map(str => +str);
  }
  constructor(private myListings:MyListingsService,private loaderService: LoaderService,private datePipe: DatePipe,private router: Router) {
   
  }

  async myUniqueHomeReportedListing() {

    try {
      this.loaderService.show();
      let response = await this.myListings.uniqueHomeReportedList();
      this.dataSource.data = response.data.Result.data;
      this.loaderService.hide();
      if(this.dataSource.data.length<=0){
        this.showToolbar=true;
      }
    } catch (e) {
      this.loaderService.hide();
    }
  }


  viewUniqueHome(city,noOfGuests,svcId){
    this.router.navigate(['/book/unique-homes-detail'],  { queryParams:{ CityName: city, NumberofGuestsInRoom:noOfGuests,svcId:svcId } });
   }
ngOnInit() {
  this.myUniqueHomeReportedListing();
}

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }
}




const ELEMENT_DATA: Element[] = [
 
];