import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { PageEvent } from '@angular/material';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { MyListingsService } from '../../my-listings.service';
import { HostedBusinessElement } from '../../myListings.Model';
import { LoaderService } from '../../../../core/loader';
@Component({
  selector: 'app-home-listingReported',
  templateUrl: './business-listingReported.component.html',
  styleUrls: ['../../sharedStyle.component.css']
})
export class BusinessListingReportedComponent implements OnInit {
  displayedColumns = ['Business Name', 'Reported Date', 'Reported Message','Action'];
  dataSource = new MatTableDataSource<HostedBusinessElement>(ELEMENT_DATA);
  pageSize = 10;
  showToolbar=false;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  pageEvent: PageEvent;
  constructor(private myListings: MyListingsService, private loaderService: LoaderService,private router: Router) {

  }
  async myBusinessReportedListing() {
    try {
      this.loaderService.show();
      let response = await this.myListings.GetBusinessHostedReportedListing();
      this.dataSource.data = response.data.Result;
      this.loaderService.hide();
      if(this.dataSource.data.length<=0){
        this.showToolbar=true;
      }
    } catch (e) {
      this.loaderService.hide();
    }
  }
  viewBusiness(city,id){
    this.router.navigate(['/book/business-detail'], { queryParams: { CityName: city,id:id } });
   }
  ngOnInit() {
    this.myBusinessReportedListing();
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }
}
const ELEMENT_DATA: HostedBusinessElement[] = [];