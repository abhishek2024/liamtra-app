import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { PageEvent } from '@angular/material';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { MyListingsService } from '../../my-listings.service'
import { FavoriteTourListing } from '../../myListings.Model'
import { LoaderService } from '../../../../core/loader';
@Component({
  selector: 'app-home-listing',
  templateUrl: './favorite-tours.component.html',
  styleUrls: ['../../sharedStyle.component.css']
})
export class FavoriteTourPackageComponent implements OnInit {

  displayedColumns = ['Tour Name', 'Host Name', 'Host Email', 'Host ContactNumber', 'source', 'destination', 'Action'];
  dataSource = new MatTableDataSource<FavoriteTourListing>(ELEMENT_DATA);
  pageSize = 10;
  showToolbar=false;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  pageEvent: PageEvent;

  constructor(private myListings: MyListingsService, private loaderService: LoaderService) {

  }

  async favoriteTourPackageListing() {
    try {
      this.loaderService.show();
      let response = await this.myListings.getAllFavoriteTourPackage();
      this.dataSource.data = response.data.Result;
      this.loaderService.hide();
      if(this.dataSource.data.length<=0){
        this.showToolbar=true;
      }
    } catch (e) {
      this.loaderService.hide();
    }
  }

  ngOnInit() {
    this.favoriteTourPackageListing();
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }
}
const ELEMENT_DATA: FavoriteTourListing[] = [];