import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { PageEvent } from '@angular/material';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { MyListingsService } from '../../my-listings.service';
import { FavoriteBusinessListing } from '../../myListings.Model';
import { LoaderService } from '../../../../core/loader';
@Component({
  selector: 'app-home-listing',
  templateUrl: './favorite-business.component.html',
  styleUrls: ['../../sharedStyle.component.css']
})
export class FavoriteBusinessComponent implements OnInit {
  displayedColumns = ['Business Name', 'Host Name', 'Host Email', 'Host Contact No.','Action'];
  dataSource = new MatTableDataSource<FavoriteBusinessListing>(ELEMENT_DATA);
  pageSize = 10;
  showToolbar=false;
  pageSizeOptions = [5, 10, 25, 100];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  pageEvent: PageEvent;
  constructor(private myListings: MyListingsService, private loaderService: LoaderService, private router: Router) {

  }
  async favoriteBusinessListing() {
    try {
      this.loaderService.show();
      let response = await this.myListings.getAllFavoriteBusiness();
      this.dataSource.data = response.data.Result;
      this.loaderService.hide();
      if(this.dataSource.data.length<=0){
        this.showToolbar=true;
      }
    } catch (e) {
      this.loaderService.hide();
    }
  }
  viewBusiness(city,id){
    this.router.navigate(['/book/business-detail'], { queryParams: { CityName: city,id:id } });
   }
  ngOnInit() {
    this.favoriteBusinessListing();
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }
}
const ELEMENT_DATA: FavoriteBusinessListing[] = [];