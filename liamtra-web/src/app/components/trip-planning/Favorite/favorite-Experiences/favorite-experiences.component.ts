import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { PageEvent } from '@angular/material';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { MyListingsService } from '../../my-listings.service';
import { FavoriteExperiencesListing } from '../../myListings.Model';
import { LoaderService } from '../../../../core/loader';
@Component({
  selector: 'app-home-listing',
  templateUrl: './favorite-experiences.component.html',
  styleUrls: ['../../sharedStyle.component.css']
})
export class FavoriteExperiencesComponent implements OnInit {
  displayedColumns = ['Experience Name', 'Host Name', 'Host Email', 'Host Contact No.','Action'];
  dataSource = new MatTableDataSource<FavoriteExperiencesListing>(ELEMENT_DATA);
  pageSize = 10;
  showToolbar=false;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  pageEvent: PageEvent;

  constructor(private myListings: MyListingsService, private loaderService: LoaderService,private router: Router) {
  }

  async favoriteExperiencesListing() {
    try {
      this.loaderService.show();
      let response = await this.myListings.getAllFavoriteExperiences();
      this.dataSource.data = response.data.Result;
      this.loaderService.hide();
      if(this.dataSource.data.length<=0){
        this.showToolbar=true;
      }
    } catch (e) {
      this.loaderService.hide();
    }
  }
  viewExperience(city,id){
    this.router.navigate(['/book/experience-detail'], { queryParams: { CityName: city,id:id } });
   }
  ngOnInit() {
    this.favoriteExperiencesListing();
  }
  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }
}
const ELEMENT_DATA: FavoriteExperiencesListing[] = [];