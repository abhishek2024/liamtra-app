import { Component, OnInit, Input, Output } from '@angular/core';
import { HostModel, HostService, HostNewModal, ServiceMultiValues } from '../../shared';
import { ServiceCategoryEnum } from '../../../../shared/enum/service-category-enum';
import { ServiceSubCategoryModel } from '../../../shared/serviceCategory.model';
import { CdKeyEnum } from '../../../../shared/enum/cd-key.enum';
import { MessageService } from '../../../../shared/message/messageService.service';



@Component({
  selector: 'app-host-exp-description',
  templateUrl: './description.component.html'
})

export class HostExperienceDescriptionComponent implements OnInit {
  @Input() hostNewModel: HostNewModal;
  public experienceData: Array<any> = [];
  public selectedExperience: Array<any> = new Array<any>();

  constructor(public hostService: HostService,
    public messageService: MessageService) {
    this.getServiceSubCategories(CdKeyEnum.ExperienceType);
  }

  ngOnInit() {
  }

  async getServiceSubCategories(cdKedy: number) {
    try {
      const result = await this.hostService.getCdkeyById(cdKedy);
      if (cdKedy === CdKeyEnum.ExperienceType) {
        this.experienceData = result.data.Result;
        this.experienceData.map(x => {
          x.id = x.id,
            x.itemName = x.displayName;
        });
      }
      if (this.hostNewModel.userServiceMultiValues.length > 0 && this.hostNewModel.id > 0) {
        this.setSelectedExperiences();
      }
    } catch (e) {
      console.log(e);
    }
  }

  checkValidation(type) {
    let errorCount = 0;
    if (!this.hostNewModel.experienceName) {
      errorCount++;
    }

    if (!this.hostNewModel.experienceDescription) {
      errorCount++;
    }

    if (errorCount > 0) {
      this.messageService.showMessage({ type: 'warning', title: 'Experience Hosting', body: 'Please fill the mandatory fields' });

      return true;

    } else {
      return false;
    }
  }

  setSelectedExperiences() {
    this.hostNewModel.userServiceMultiValues.map(userService => {
      this.experienceData.map(data => {
        if (userService.cdKeyValueId === data.id) {
          // amenty.userServiceId = this.hostTour.userServiceId;
          this.selectedExperience.splice(this.selectedExperience.length, 0, data);
        }
      });
    });
  }

  onExperienceSelect(item: any) {
    const filterItem: ServiceMultiValues = {
      cdKeyValueId: item.id,
      id: 0,
      cdKeyValue: '',
      userServiceId: 0,
      isDeleted: false
    };
    this.hostNewModel.userServiceMultiValues = this.hostNewModel.userServiceMultiValues.concat(filterItem);
  }

  onExperienceDeSelect(item: any) {
    const obj = this.hostNewModel.userServiceMultiValues.filter(x => x.cdKeyValueId === item.id)[0];
    const index = this.hostNewModel.userServiceMultiValues.indexOf(obj);
    this.hostNewModel.userServiceMultiValues[index].isDeleted = true;
  }

  onExperienceSelectAll(items: any) {
    this.onExperienceDeSelectAll();
    items.forEach(element => {
      const filterItem: ServiceMultiValues = {
        cdKeyValueId: element.id,
        id: 0,
        cdKeyValue: '',
        userServiceId: 0,
        isDeleted: false
      };
      this.hostNewModel.userServiceMultiValues.splice(this.hostNewModel.userServiceMultiValues.length, 0, filterItem);
    });
  }

  onExperienceDeSelectAll() {
    const arr = this.hostNewModel.userServiceMultiValues.map(x => Object.assign({}, x));
    arr.forEach((item, index) => {
      const hostIndex = this.hostNewModel.userServiceMultiValues.findIndex(hostItem => hostItem.cdKeyValueId === item.cdKeyValueId);
      this.hostNewModel.userServiceMultiValues[hostIndex].isDeleted = true;
    });
  }

}
