import { Component, OnInit, ViewContainerRef, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HostModel, HostService, HostNewModal } from '../shared';
import { LoaderService } from '../../../core/loader/loader.service';
import { DetailComponent } from '../shared/detail/detail.component';
import { LocationComponent } from '../../shared/location/location.component';
import { HostExperienceDescriptionComponent } from './description/description.component';
import { LocalStorageService } from '../../../shared/services/local-storage.service';
import { ServiceCategoryEnum } from '../../../shared/enum/service-category-enum';
import { MessageService } from '../../../shared/message/messageService.service';

@Component({
  selector: 'app-experience',
  templateUrl: './experience.component.html'
})
export class HostExperienceComponent implements OnInit {
  isTermAccepted = false;
  stepNumber: number = 1;
  @ViewChild(DetailComponent) detail: DetailComponent;
  @ViewChild(HostExperienceDescriptionComponent) description: HostExperienceDescriptionComponent;
  @ViewChild(LocationComponent) location: LocationComponent;
  experienceModel: HostNewModal = new HostNewModal();
  mapSearchElement: any;
  mapCss: string = 'hostExperinceMap';
  hostTypeName: string = 'Host Name';
  modal: boolean = false;
  modalStatus: string = '';
  termAndConditionmodal: boolean = false;
  serviceTypeId = ServiceCategoryEnum.HostExperience;
  isMarkerDraggable: boolean = true;

  constructor(protected hostService: HostService,
    private loaderService: LoaderService,
    private route: ActivatedRoute,
    private localStorageService: LocalStorageService,
    protected router: Router,
    private messageService: MessageService) {
    if (this.route.snapshot.params.id) {
      this.experienceModel.id = this.route.snapshot.params.id !== 0 ? this.route.snapshot.params.id : 0;
    }

    if (this.experienceModel.id !== 0) {
      this.getExperienceById();
    }
  }

  ngOnInit() {
    this.checkUserLogin();
  }


  async getExperienceById() {
    try {
      this.loaderService.show();
      const UserId = +localStorage.getItem('userId');
      const response = await this.hostService.getExperience(this.experienceModel.id);
      this.experienceModel = response.data.Result;
      this.loaderService.hide();
    } catch (e) {
      this.loaderService.hide();
    }
  }

  getMapSearch(item) {
    // if (!item.cordinateEvent) {
    this.mapSearchElement = item;
    // }
  }

  async saveExperience() {
    if (!this.isTermAccepted) {
      this.messageService.showMessage({ type: 'warning', title: '', body: 'Please accept the terms & condition' });

      return;
    }
    try {
      this.loaderService.show();
      const result = await this.hostService.saveHost(this.experienceModel);
      this.loaderService.hide();
      this.router.navigate(['host/host-thank-you']);
    } catch (e) {
      console.log(e);
      this.loaderService.hide();
    }
  }

  backButton() {
    this.stepNumber = this.stepNumber - 1;
  }

  nextButton() {
    let errorOnPage = false;
    if (this.stepNumber === 1) {
      errorOnPage = this.detail.checkValidation('all');
    }
    if (this.stepNumber === 2) {
      errorOnPage = this.location.checkValidation('all');
    }
    if (this.stepNumber === 3) {
      errorOnPage = this.description.checkValidation('all');
    }

    if (!errorOnPage) {
      let isLogin = this.detail.checkLogin();
      if (isLogin) {
        this.stepNumber = this.stepNumber + 1;
      } else {
        this.messageService.showMessage({ type: 'warning', title: '', body: 'Please Login before host any unique home' });

        this.checkUserLogin();
      }
    }
  }

  tabSwitch(stepNumber) {
    if (this.stepNumber > stepNumber) {
      this.stepNumber = stepNumber;
    }
  }

  getCordinatesEvent(obj) {
    this.experienceModel.latitude = obj.latitude;
    this.experienceModel.longitude = obj.longitude;
  }

  modalPopupEvent(result) {
    this.modal = result;
  }

  async checkUserLogin() {
    try {
      const access_token = this.localStorageService.getAccessToken();
      if (access_token) {
        return true;
      } else {
        this.modalStatus = 'signIn';
        this.modal = true;
        return false;
      }
    } catch (e) { }
  }
  getTermsAndConditions() {
    this.termAndConditionmodal = true;
  }

  termsAndConditionsPopupEvent(event) {
    this.termAndConditionmodal = false;
  }


}
