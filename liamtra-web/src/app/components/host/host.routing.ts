import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { HostExperienceComponent } from './experience/experience.component';
import { UniqueHomeComponent } from './unique-home/unique-home.component';
import { HostBusinessComponent } from './business';
import { TaxiComponent } from './taxi/taxi.component';
import { HostThankYouComponent } from './shared';
import { TourComponent } from './tour';

export const routes: Routes = [
  {
    path: '',
    children: [
      { path: 'experience', component: HostExperienceComponent },
      { path: 'experience/:id', component: HostExperienceComponent },
      { path: 'property', component: UniqueHomeComponent },
      { path: 'business', component: HostBusinessComponent },
      { path: 'business/:id', component: HostBusinessComponent },
      { path: 'taxi', component: TaxiComponent },
      { path: 'host-thank-you', component: HostThankYouComponent },
      { path: 'tour', component: TourComponent },
      { path: 'tour/:id', component: TourComponent },

    ]
  }
];

export const HostRouting: ModuleWithProviders = RouterModule.forChild(routes);
