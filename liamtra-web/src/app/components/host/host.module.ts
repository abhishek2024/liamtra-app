
import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import {
  HostExperienceComponent,
  HostExperienceDescriptionComponent,
} from './experience';
import { UniqueHomeComponent } from './unique-home/unique-home.component';
import { HostThankYouComponent } from './shared';
import { DetailComponent } from './shared/detail/detail.component';
import { ListingComponent } from './unique-home/listing/listing.component';
import { HighlightsComponent } from './unique-home/highlights/highlights.component';
import {
  HostBusinessComponent,
  HostBusinessVendorDescriptionComponent
} from './business';
import { TaxiComponent, VehicleVendorDetailComponent } from './taxi';
import { HostRouting } from './host.routing';
import { HostService } from './shared/host.service';
import { TourComponent, HostTourContentComponent, HostTourDetailComponent, HostTourStayPlanComponent, HostTourService } from './tour';
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';

@NgModule({
  declarations: [
    HostExperienceComponent,
    HostExperienceDescriptionComponent,
    UniqueHomeComponent,
    DetailComponent,
    HostThankYouComponent,
    ListingComponent,
    HighlightsComponent,
    HostBusinessComponent,
    HostBusinessVendorDescriptionComponent,
    TaxiComponent,
    VehicleVendorDetailComponent,
    TourComponent,
    HostTourContentComponent,
    HostTourDetailComponent,
    HostTourStayPlanComponent
  ],
  imports: [
    SharedModule,
    HostRouting,
    FroalaEditorModule.forRoot(),
    FroalaViewModule.forRoot()
  ],
  providers: [
    HostService,
    HostTourService
  ],
  exports: [
    SharedModule

  ]
})
export class HostModule { }
