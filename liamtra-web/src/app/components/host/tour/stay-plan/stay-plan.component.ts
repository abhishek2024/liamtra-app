import { Component, OnInit, Input, ViewContainerRef } from '@angular/core';
import { HostTourService, HostTour, UserServiceMultiValue, TourStayPlan } from '../shared';
import { CdKeyEnum } from '../../../../shared/enum/cd-key.enum';
import { LoaderService } from '../../../../core/loader';
import { MessageService } from '../../../../shared/message/messageService.service';



@Component({
  selector: 'app-host-tour-stay-plan',
  templateUrl: './stay-plan.component.html',
  styleUrls: ['./stay-plan.component.css']
})

export class HostTourStayPlanComponent implements OnInit {

  @Input() hostTour: HostTour;
  public dropdownSettings = {};
  amenties: Array<any> = [];
  selectedAmenties: Array<any> = [];

  constructor(private hostTourService: HostTourService,
    private loaderService: LoaderService,
    private messageService: MessageService) {
    this.getAmenties();
  }

  ngOnInit() {
    this.dropdownSettings = {
      singleSelection: false,
      text: 'Select',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: true,
      classes: 'myclass custom-class',
      searchAutofocus: true
    };

    if (this.hostTour.id === 0) {
      this.addStayPlan();
    }
  }

  async getAmenties() {
    try {
      this.loaderService.show();
      let response = await this.hostTourService.getAmenties(CdKeyEnum.Amenities);
      this.amenties = response.data.Result;
      this.amenties.map(x => {
        x.id = x.id,
          x.itemName = x.displayName
      });
      if (this.hostTour.userServiceMultiValues.length > 0 && this.hostTour.id > 0) {
        this.setSelectedAmenties();
      }
      this.loaderService.hide();
    } catch (e) {
      this.loaderService.hide();
    }
  }

  setSelectedAmenties() {
    this.hostTour.userServiceMultiValues.map(userService => {
      this.amenties.map(amenty => {
        if (userService.cdKeyValueId === amenty.id) {
          // amenty.userServiceId = this.hostTour.userServiceId;
          this.selectedAmenties.splice(this.selectedAmenties.length, 0, amenty);
        }
      });
    });
  }

  addStayPlan() {
    let error = this.checkValidation('stayPlan');
    if (!error) {
      let stayPlan = new TourStayPlan;
      this.hostTour.tourStayPlans.splice(this.hostTour.tourStayPlans.length, 0, stayPlan);
    } else {
      this.messageService.showMessage({ type: 'warning', title: '', body: 'Please fill the mandatory field before add new plan' });
    }
  }

  deleteStayPlan(index) {
    if (index != 0) {
      // this.hostTour.tourStayPlans.splice(index, 1);
      this.hostTour.tourStayPlans[index].isDeleted = true;
    } else {
      //cant delete single value
    }
  }

  onAmentySelect(item: any) {
    const filterItem: UserServiceMultiValue = {
      id: 0,
      userServiceId: this.hostTour.userServiceId,
      cdKeyValueId: item.id,
      isDeleted: false
    }
    const obj = this.hostTour.userServiceMultiValues.filter(x => x.cdKeyValueId === item.id)[0];
    const index = this.hostTour.userServiceMultiValues.indexOf(obj);
    if (index === -1) {
      this.hostTour.userServiceMultiValues = this.hostTour.userServiceMultiValues.concat(filterItem);
    } else {
      this.hostTour.userServiceMultiValues[index].isDeleted = false;
    }
  }

  onAmentyDeSelect(item: any) {
    const obj = this.hostTour.userServiceMultiValues.filter(x => x.cdKeyValueId === item.id)[0];
    const index = this.hostTour.userServiceMultiValues.indexOf(obj);
    this.hostTour.userServiceMultiValues[index].isDeleted = true;
  }

  onAmentySelectAll(items: any) {
    this.onAmentyDeSelectAll();
    items.forEach(element => {
      const filterItem: UserServiceMultiValue = {
        id: 0,
        userServiceId: this.hostTour.userServiceId,
        cdKeyValueId: element.id,
        isDeleted: false
      }
      const obj = this.hostTour.userServiceMultiValues.filter(x => x.cdKeyValueId === element.id)[0];
      const index = this.hostTour.userServiceMultiValues.indexOf(obj);
      if (index === -1) {
        this.hostTour.userServiceMultiValues.splice(this.hostTour.userServiceMultiValues.length, 0, filterItem);
      } else {
        this.hostTour.userServiceMultiValues[index].isDeleted = false;
      }
    });
  }

  onAmentyDeSelectAll() {
    const arr = this.hostTour.userServiceMultiValues.map(x => Object.assign({}, x));
    arr.forEach((item, index) => {
      let hostIndex = this.hostTour.userServiceMultiValues.findIndex(hostItem => hostItem.cdKeyValueId === item.cdKeyValueId)
      this.hostTour.userServiceMultiValues[hostIndex].isDeleted = true;
    });
  }

  checkValidation(type) {
    let error = false;
    this.hostTour.tourStayPlans.forEach(item => {
      if (item.totalNights === '' || item.cityName === '') {
        error = true;
        return;
      }
    });
    if (type !== 'stayPlan') {
      if (this.selectedAmenties.length === 0) {
        error = true;
      }
    }
    return error;
  }
}
