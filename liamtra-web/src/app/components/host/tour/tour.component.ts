import { Component, OnInit, ViewContainerRef, ViewChild } from '@angular/core';
import { LocalStorageService } from '../../../shared/services/local-storage.service';
import { Router, ActivatedRoute } from '@angular/router';
import { HostTour, HostTourService } from './shared';
import { ServiceCategoryEnum } from '../../../shared/enum/service-category-enum';
import { LoaderService } from '../../../core/loader';
import { HostTourDetailComponent } from './detail/detail.component';
import { HostTourContentComponent } from './content/content.component';
import { HostTourStayPlanComponent } from './stay-plan/stay-plan.component';
import { MessageService } from '../../../shared/message/messageService.service';


@Component({
  selector: 'app-host-tour',
  templateUrl: './tour.component.html'
})

export class TourComponent implements OnInit {
  stepNumber: number = 1;
  modal: boolean = false;
  modalStatus: string = '';
  termAndConditionmodal: boolean = false;
  serviceTypeId = ServiceCategoryEnum.HostTour;
  hostTour: HostTour = new HostTour();
  @ViewChild(HostTourDetailComponent) detail: HostTourDetailComponent;
  @ViewChild(HostTourContentComponent) content: HostTourContentComponent;
  @ViewChild(HostTourStayPlanComponent) stayPlan: HostTourStayPlanComponent;


  constructor(
    private localStorageService: LocalStorageService,
    private loaderService: LoaderService,
    private hostTourService: HostTourService,
    private router: Router,
    private route: ActivatedRoute,
    private messageService: MessageService) {
    if (this.route.snapshot.params.id) {
      this.hostTour.id = this.route.snapshot.params.id !== 0 ? this.route.snapshot.params.id : 0;
    }

    if (this.hostTour.id !== 0) {
      this.getTourById();
    }
  }

  ngOnInit() {
    this.checkUserLogin();
  }

  async getTourById() {
    try {
      this.loaderService.show();
      const response = await this.hostTourService.getTourById(this.hostTour.id);
      this.hostTour = response.data.Result;
      this.loaderService.hide();
    } catch (e) {
      this.loaderService.hide();
    }
  }

  tabSwitch(stepNumber) {
    if (this.stepNumber > stepNumber) {
      this.stepNumber = stepNumber;
    }
  }

  backButton() {
    this.stepNumber = this.stepNumber - 1;
  }

  nextButton() {
    let errorOnPage = false;
    if (this.stepNumber === 1) {
      errorOnPage = this.detail.checkValidation();
    }

    if (this.stepNumber === 2) {
      errorOnPage = this.stayPlan.checkValidation('all');
    }

    if (this.stepNumber === 3) {
      errorOnPage = this.content.checkValidation();
    }

    if (!errorOnPage) {
      let isLogin = true//this.detail.checkLogin();
      if (isLogin) {
        this.stepNumber = this.stepNumber + 1;
      } else {
        this.messageService.showMessage({ type: 'warning', title: 'Tour', body: 'Please Login before host any unique home' });

        this.checkUserLogin();
      }
    } else {
      this.messageService.showMessage({ type: 'warning', title: 'Tour', body: 'Please fill the mandatory field.' });

    }
  }

  modalPopupEvent(result) {
    this.modal = result;
  }

  checkUserLogin() {
    try {
      const access_token = this.localStorageService.getAccessToken();
      if (access_token) {
        return true;
      } else {
        this.modalStatus = 'signIn';
        this.modal = true;
        return false;
      }
    } catch (e) { }
  }

  async submit() {
    if (!this.hostTour.isTermAccepted) {
      this.messageService.showMessage({ type: 'warning', title: '', body: 'Please agree the terms & condition' });

      return;
    }
    try {
      this.loaderService.show();
      const response = await this.hostTourService.saveHostTour(this.hostTour);
      this.loaderService.hide();
      this.router.navigate(['host/host-thank-you']);
    } catch (e) {
      this.loaderService.hide();
    }
  }

  getTermsAndConditions() {
    this.termAndConditionmodal = true;
  }

  termsAndConditionsPopupEvent(event) {
    this.termAndConditionmodal = false;
  }

}
