import { Component, OnInit, Input } from '@angular/core';
import { HostTour, TourContent, HostTourService } from '../shared';
import { LoaderService } from '../../../../core/loader';

@Component({
  selector: 'app-host-tour-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.css']
})

export class HostTourContentComponent implements OnInit {

  @Input() hostTour = new HostTour();
  contents: Array<any> = Array<any>();
  froalaText: any = 'My test';
  indexNo: number = 0;
  selectedItem:string;
  constructor(private hostTourService: HostTourService,
    private loaderService: LoaderService, ) {
  }

  ngOnInit() {
    // this.contents = [{ fieldTitle: 'Overview', id: 1 },
    // { fieldTitle: 'Inclusion', id: 2 },
    // { fieldTitle: 'Exclusion', id: 3 },
    // { fieldTitle: 'Payment Policy', id: 4 },
    // { fieldTitle: 'Cancellation Policy', id: 5 },
    // { fieldTitle: 'About Dharamshala', id: 6 }]


    this.getContents();
  }

  async getContents() {
    try {
      this.loaderService.show();
      let response = await this.hostTourService.getContents();
      this.contents = response.data.Result;
      this.addContentByContentId(this.contents[0]);
      this.loaderService.hide();
    } catch (e) {
      this.loaderService.hide();
    }
  }

  addContentByContentId(content) {
    this.selectedItem = content;
    let index = this.hostTour.tourContents.findIndex(i => i.tourContentFieldId === content.id)
    if (index === -1) {
      this.froalaText = '';
      let tempContent: TourContent = { id: 0, tourId: 0, tourContentFieldId: content.id, contentValue: '' };
      this.hostTour.tourContents.splice(this.hostTour.tourContents.length, 0, tempContent)
      this.indexNo = this.hostTour.tourContents.length - 1;
    } else {
      this.indexNo = index;
    }
  }

  checkValidation() {
    let error = false;
    this.hostTour.tourContents.forEach(content => {
      if (content.contentValue === '') {
        error = true;
      }
    });

    return error;
  }

}
