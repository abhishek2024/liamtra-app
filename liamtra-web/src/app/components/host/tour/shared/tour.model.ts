export class HostTour {
  id: number = 0;
  userServiceId: number = 0;
  title: string = '';
  sellerId: number = 0;
  startDate: Date = new Date();
  endDate: Date = new Date();
  source: string = '';
  destination: string = '';
  totalDays: number;
  totalNights: number;
  totalSeats: number;
  totalRooms: number;
  totalAdults: number;
  stayDuration: number;
  packagePrice: number;
  gst: number;
  isTermAccepted: boolean = false
  tourStayPlans: Array<TourStayPlan> = new Array<TourStayPlan>();
  tourContents: Array<TourContent> = new Array<TourContent>();
  userServiceAssets: Array<TourAttachment> = new Array<TourAttachment>();
  userServiceMultiValues: Array<UserServiceMultiValue> = new Array<UserServiceMultiValue>();
}


export class UserServiceMultiValue {
  id: number = 0;
  userServiceId: number = 0;
  cdKeyValueId: number = 0;
  isDeleted: boolean = false;
}

export class TourStayPlan {
  isDeleted: boolean = false;
  id: number = 0;
  tourId: number = 0;
  cityName: string = '';
  totalNights: string = '';
}

export class TourContent {
  id: number = 0;
  tourId: number = 0;
  tourContentFieldId: number = 0;
  contentValue: any;
}



export class TourAttachment {
  id: number = 0;
  userServiceId: number = 0;
  assetTypeId: number = 0;
  fileName: string = '';
  isActive: boolean = false;
  isCoverPhoto: boolean = false;
  attachmentDataBase64: string = '';
  attachmentData: string = '';
  attachmentUrl: string = '';
  isDeleted: boolean = false;
}