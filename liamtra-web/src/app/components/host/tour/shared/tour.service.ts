import { HttpModule, Http } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/Rx';
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/toPromise';
import {
  ObjectResponseModel,
  PostObjectResponseModel,
  ArrayResponseModel,
  AsyncArrayPromiseHandler,
  AsyncObjectPromiseHandler
} from '../../../../shared/models/base-data.model';
import { ApiUrl } from '../../../../api.service';
import { HostTour, UserServiceMultiValue } from './tour.model';

@Injectable()
export class HostTourService {
  constructor(private http: Http) {
  }

  async getAmenties(cdkId): Promise<ArrayResponseModel<UserServiceMultiValue>> {
    const promise = this.http.get(ApiUrl.HOST_URI + 'CdKeyValue/GetByCDKeyID/' + cdkId);
    return new AsyncArrayPromiseHandler<UserServiceMultiValue>(promise.toPromise());
  }

  async getContents(): Promise<ArrayResponseModel<UserServiceMultiValue>> {
    const promise = this.http.get(ApiUrl.HOST_URI + 'TourContentField');
    return new AsyncArrayPromiseHandler<UserServiceMultiValue>(promise.toPromise());
  }

  async getTourById(id): Promise<ObjectResponseModel<HostTour>> {
    const promise = this.http.get(ApiUrl.HOST_URI + 'Tour/get/' + id);
    return new AsyncObjectPromiseHandler<HostTour>(promise.toPromise());
  }

  saveHostTour(data: HostTour): Promise<ObjectResponseModel<HostTour>> {
    let promise = null;
    if (data.id !== 0) {
      promise = this.http
        .put(ApiUrl.HOST_URI + 'Tour', data);
      return new AsyncObjectPromiseHandler<HostTour>(promise.toPromise());
    } else {
      promise = this.http
        .post(ApiUrl.HOST_URI + 'Tour', data);
      return new AsyncObjectPromiseHandler<HostTour>(promise.toPromise());
    }

  }
}
