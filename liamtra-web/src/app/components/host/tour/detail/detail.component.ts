import { Component, OnInit, Input } from '@angular/core';
import { HostTourService, HostTour } from '../shared';
import { MessageService } from '../../../../shared/message/messageService.service';

@Component({
  selector: 'app-host-tour-detail',
  templateUrl: './detail.component.html'
})

export class HostTourDetailComponent implements OnInit {

  @Input() hostTour: HostTour;
  minDateForDatepicker = new Date();

  constructor(private hostTourService: HostTourService,
    private messageService: MessageService) {
  }

  ngOnInit() { }

  checkValidation() {
    let errorCount = 0;
    if (this.hostTour.title === '' || this.hostTour.source === '' || this.hostTour.destination === ''
      || this.hostTour.totalDays === 0 || this.hostTour.totalNights === 0 || this.hostTour.totalSeats === 0
      || this.hostTour.totalRooms === 0 || this.hostTour.totalAdults === 0 || this.hostTour.stayDuration === 0
      || this.hostTour.packagePrice === 0 || this.hostTour.gst === 0) {
      errorCount++;
    }

    if (this.hostTour.totalSeats < this.hostTour.totalAdults) {
      this.messageService.showMessage({ type: 'warning', title: 'Tour', body: 'Total seats shold not be less than total adults' })
      errorCount++;
    }

    if (errorCount > 0) {
      return true;
    } else {
      return false;
    }
  }

}
