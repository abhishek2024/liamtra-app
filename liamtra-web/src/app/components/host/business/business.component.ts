import { Component, OnInit, ViewContainerRef, ViewChild } from '@angular/core';

import { Router, ActivatedRoute } from '@angular/router';
import { HostModel, HostService, HostNewModal } from '../shared';
import { DetailComponent } from '../shared/detail/detail.component';
import { HostBusinessVendorDescriptionComponent } from './vendor-description/vendor-description.component';
import { LocationComponent } from '../../shared/location/location.component';
import { LoaderService } from '../../../core/loader/loader.service';
import { LocalStorageService } from '../../../shared/services/local-storage.service';
import { ServiceCategoryEnum } from '../../../shared/enum/service-category-enum';
import { MessageService } from '../../../shared/message/messageService.service';

@Component({
  selector: 'app-business',
  templateUrl: './business.component.html'
})
export class HostBusinessComponent implements OnInit {
  isTermAccepted = false;
  businessModel: HostNewModal = new HostNewModal();
  @ViewChild(DetailComponent) detail: DetailComponent;
  @ViewChild(LocationComponent) location: LocationComponent;
  @ViewChild(HostBusinessVendorDescriptionComponent) description: HostBusinessVendorDescriptionComponent;
  stepNumber: number = 1;
  mapSearchElement: any;
  mapCss: string = 'hostExperinceMap';
  hostTypeName: string = 'Host Name';
  modal: boolean = false;
  modalStatus: string = '';
  termAndConditionmodal: boolean = false;
  serviceTypeId = ServiceCategoryEnum.HostBusiness;
  isMarkerDraggable: boolean = true;

  constructor(private loaderService: LoaderService,
    private route: ActivatedRoute,
    protected router: Router,
    private hostService: HostService,
    private messageService: MessageService,
    private localStorageService: LocalStorageService) {
    if (this.route.snapshot.params.id) {
      this.businessModel.id = +this.route.snapshot.params.id !== 0 ? this.route.snapshot.params.id : 0;
    }
    if (this.businessModel.id !== 0) {
      this.getBusinessDetailById();
    }
  }

  ngOnInit() {
    this.checkUserLogin();
  }

  async getBusinessDetailById() {
    try {
      this.loaderService.show();
      const userId = localStorage.getItem('userId');
      const response = await this.hostService.getBusinessDetail(this.businessModel.id);
      this.businessModel = response.data.Result;
      this.loaderService.hide();
    } catch (e) {
      this.loaderService.hide();
    }
  }



  getMapSearch(item) {
    if (!item.cordinateEvent) {
      this.mapSearchElement = item;
    }
  }

  backButton() {
    this.stepNumber = this.stepNumber - 1;
  }

  nextButton() {
    let errorOnPage = false;
    if (this.stepNumber === 1) {
      errorOnPage = this.detail.checkValidation('all');
    }

    if (this.stepNumber === 2) {
      errorOnPage = this.location.checkValidation('all');
    }

    if (this.stepNumber === 3) {
      errorOnPage = this.description.checkValidation('all');
    }

    if (!errorOnPage) {
      let isLogin = this.detail.checkLogin();
      if (isLogin) {
        this.stepNumber = this.stepNumber + 1;
      } else {
        this.messageService.showMessage({ type: 'warning', title: '', body: 'Please Login before host any unique home' });

        this.checkUserLogin();
      }
    }
  }

  tabSwitch(stepNumber) {
    // if (this.stepNumber > stepNumber) {
    this.stepNumber = stepNumber;
    // }
  }

  getCordinatesEvent(obj) {
    this.businessModel.latitude = obj.latitude;
    this.businessModel.longitude = obj.longitude;
  }

  async submit() {
    if (!this.isTermAccepted) {
      this.messageService.showMessage({ type: 'warning', title: '', body: 'Please accept the terms & condition' });

      return;
    }
    try {
      this.loaderService.show();
      const result = await this.hostService.saveBusinessHost(this.businessModel);
      this.loaderService.hide();
      this.router.navigate(['host/host-thank-you']);
    } catch (e) {
      console.log(e);
      this.loaderService.hide();
    }
  }

  modalPopupEvent(result) {
    this.modal = result;
  }

  async checkUserLogin() {
    try {
      const access_token = this.localStorageService.getAccessToken();
      if (access_token) {
        return true;
      } else {
        this.modalStatus = 'signIn';
        this.modal = true;
        return false;
      }
    } catch (e) { }
  }


  getTermsAndConditions() {
    this.termAndConditionmodal = true;
  }

  termsAndConditionsPopupEvent(event) {
    this.termAndConditionmodal = false;
  }

}
