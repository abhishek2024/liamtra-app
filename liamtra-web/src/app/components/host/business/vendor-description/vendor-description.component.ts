import { Component, OnInit, Input } from '@angular/core';
import { HostModel, HostService, HostNewModal, ServiceMultiValues } from '../../shared';
import { ServiceCategoryEnum } from '../../../../shared/enum/service-category-enum';
import { CdKeyEnum } from '../../../../shared/enum/cd-key.enum';
import { MessageService } from '../../../../shared/message/messageService.service';

@Component({
  selector: 'app-vendor-description',
  templateUrl: './vendor-description.component.html'
})
export class HostBusinessVendorDescriptionComponent implements OnInit {
  @Input() businessModel: HostNewModal;
  public serviceData: Array<any> = [];
  public selectedService: Array<any> = new Array<any>();

  constructor(public hostService: HostService,
    public messageService: MessageService) { }

  ngOnInit() {
    this.getServiceSubCategories(CdKeyEnum.BusinessType);
  }

  async getServiceSubCategories(cdKeyId: number) {
    try {

      const result = await this.hostService.getCdkeyById(cdKeyId);

      this.serviceData = result.data.Result;
      this.serviceData.map(x => {
        x.id = x.id,
          x.itemName = x.displayName;
      });

      if (this.businessModel.userServiceMultiValues.length > 0 && this.businessModel.id > 0) {
        this.setSelectedBusiness();
      }

    } catch (e) {
      console.log(e);
    }
  }

  setSelectedBusiness() {
    this.businessModel.userServiceMultiValues.map(userService => {
      this.serviceData.map(data => {
        if (userService.cdKeyValueId === data.id) {
          // amenty.userServiceId = this.hostTour.userServiceId;
          this.selectedService.splice(this.selectedService.length, 0, data);
        }
      });
    });
  }


  checkValidation(type) {
    let errorCount = 0;
    if (!this.businessModel.businessName) {
      errorCount++;
    }
    if (!this.businessModel.businessDescription) {
      errorCount++;
    }

    if (errorCount > 0) {
      if (type === 'all') {
        this.messageService.showMessage({ type: 'warning', title: '', body: 'Please fill the mandatory fields' });
      }
      return true;
    } else {
      return false;
    }
  }

  onExperienceSelect(item: any) {
    const filterItem: ServiceMultiValues = {
      cdKeyValueId: item.id,
      id: 0,
      cdKeyValue: '',
      userServiceId: 0,
      isDeleted: false
    };
    const obj = this.businessModel.userServiceMultiValues.filter(x => x.cdKeyValueId === item.id)[0];
    const index = this.businessModel.userServiceMultiValues.indexOf(obj);
    if (index === -1) {
      this.businessModel.userServiceMultiValues = this.businessModel.userServiceMultiValues.concat(filterItem);
    } else {
      this.businessModel.userServiceMultiValues[index].isDeleted = false;
    }
  }

  onExperienceDeSelect(item: any) {
    const obj = this.businessModel.userServiceMultiValues.filter(x => x.cdKeyValueId === item.id)[0];
    const index = this.businessModel.userServiceMultiValues.indexOf(obj);
    this.businessModel.userServiceMultiValues[index].isDeleted = true;
  }

  onExperienceSelectAll(items: any) {
    this.onExperienceDeSelectAll();
    items.forEach(element => {
      const filterItem: ServiceMultiValues = {
        cdKeyValueId: element.id,
        id: 0,
        cdKeyValue: '',
        userServiceId: 0,
        isDeleted: false
      };
      const obj = this.businessModel.userServiceMultiValues.filter(x => x.cdKeyValueId === element.id)[0];
      const index = this.businessModel.userServiceMultiValues.indexOf(obj);
      if (index === -1) {
        this.businessModel.userServiceMultiValues = this.businessModel.userServiceMultiValues.concat(filterItem);
      } else {
        this.businessModel.userServiceMultiValues[index].isDeleted = false;
      }
    });
  }

  onExperienceDeSelectAll() {
    const arr = this.businessModel.userServiceMultiValues.map(x => Object.assign({}, x));
    arr.forEach((item, index) => {
      const hostIndex = this.businessModel.userServiceMultiValues.findIndex(hostItem => hostItem.cdKeyValueId === item.cdKeyValueId);
      this.businessModel.userServiceMultiValues[hostIndex].isDeleted = true;
    });
  }
}
