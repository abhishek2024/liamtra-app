import { Component, ViewContainerRef, OnInit, Input } from '@angular/core';
import { HostModel, HostService, HostNewModal, ServiceMultiValues } from '../../shared';
import { ServiceCategoryEnum } from '../../../../shared/enum/service-category-enum';
import { ServiceSubCategoryModel } from '../../../shared/serviceCategory.model';
import { LocalStorageService } from '../../../../shared/services/local-storage.service';
import { MessageService } from '../../../../shared/message/messageService.service';
import { CdKeyEnum } from '../../../../shared/enum/cd-key.enum';

@Component({
  selector: 'app-host-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})

export class DetailComponent implements OnInit {
  @Input() isShow: any;
  @Input() hostModel: HostModel;
  @Input() hostNewModal: HostNewModal;
  @Input() hostTypeName: string = '';
  emailPattern = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  phonenoPattern = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
  languages: Array<any> = [];
  public selectedLanguages: Array<any> = new Array<any>();
  isContactInvalid: boolean = false;
  isEmailInvalid: boolean = false;
  constructor(public hostService: HostService,
    private localStorageService: LocalStorageService,
    public messageService: MessageService) {
  }

  ngOnInit() {
    this.getServiceCategories();
    this.getServiceSubCategories(ServiceCategoryEnum.Languages);
    let user = this.localStorageService.getCurrentUser().User;
    if (this.hostModel) {
      this.hostModel.eMailId = user.UserEmail;
      this.hostModel.contactNo = user.UserContactNo;
    }
    if (this.hostNewModal && user.UserEmail) {
      this.hostNewModal.emailId = user.UserEmail;
      this.hostNewModal.contactNo = user.UserContactNo;
    }

  }

  async getServiceCategories() {
    try {
      await this.hostService.getServiceCategoryList();
    } catch (e) {
      // console.log(e);
    }
  }

  async getServiceSubCategories(id: number) {
    if (this.hostNewModal) {
      try {
        const newId = CdKeyEnum.Languages;
        const response = await this.hostService.getCdkeyById(newId);
        this.languages = response.data.Result;
        this.languages.map(x => {
          x.svcCatgId = x.cdKeyId,
            x.itemName = x.displayName,
            x.id = x.id;
        });
        if (this.hostNewModal.userServiceMultiValues.length > 0 && this.hostNewModal.id > 0) {
          this.setSelectedLanguages();
        }
      } catch (e) {
        // console.log(e);
      }
    } else {
      try {
        const response = await this.hostService.getServiceCategoryById(id);
        this.languages = response.data.Result;
        this.languages.map(x => {
          x.svcCatgId = x.svcCatgId,
            x.itemName = x.svcSCatgDesc,
            x.id = x.svcSCatgId;
        });
      } catch (e) {
        // console.log(e);
      }
    }

  }

  setSelectedLanguages() {
    this.hostNewModal.userServiceMultiValues.map(userService => {
      this.languages.map(data => {
        if (userService.cdKeyValueId === data.id) {
          this.selectedLanguages.splice(this.selectedLanguages.length, 0, data);
        }
      });
    });
  }



  public onLanguageItemSelect(item: any) {
    if (this.hostNewModal) {
      const filterItem: ServiceMultiValues = {
        cdKeyValueId: item.id,
        id: 0,
        cdKeyValue: '',
        userServiceId: 0,
        isDeleted: false
      };
      const obj = this.hostNewModal.userServiceMultiValues.filter(x => x.cdKeyValueId === item.id)[0];
      const index = this.hostNewModal.userServiceMultiValues.indexOf(obj);
      if (index === -1) {
        this.hostNewModal.userServiceMultiValues = this.hostNewModal.userServiceMultiValues.concat(filterItem);
      } else {
        this.hostNewModal.userServiceMultiValues[index].isDeleted = false;
      }
    } else {
      this.hostModel.hostServiceLinkingSubCategories = this.hostModel.hostServiceLinkingSubCategories.concat(this.selectedLanguages);
      this.hostModel.hostServiceLinkingSubCategories = Array.from(new Set(this.hostModel.hostServiceLinkingSubCategories));
    }

  }



  public onLanguageItemDeSelect(item: any) {
    if (this.hostNewModal) {
      const obj = this.hostNewModal.userServiceMultiValues.filter(x => x.cdKeyValueId === item.id)[0];
      const index = this.hostNewModal.userServiceMultiValues.indexOf(obj);
      // this.hostNewModal.userServiceMultiValues.splice(index, 1);
      this.hostNewModal.userServiceMultiValues[index].isDeleted = true;
    } else {
      const obj = this.hostModel.hostServiceLinkingSubCategories.filter(x => x.svcSCatgId === item.id)[0];
      const index = this.hostModel.hostServiceLinkingSubCategories.indexOf(obj);
      this.hostModel.hostServiceLinkingSubCategories.splice(index, 1);
    }

  }

  public onLanguageSelectAll(items: any) {
    this.onLanguageDeSelectAll();
    if (this.hostNewModal) {
      items.forEach(element => {
        const filterItem: ServiceMultiValues = {
          cdKeyValueId: element.id,
          id: 0,
          cdKeyValue: '',
          userServiceId: 0,
          isDeleted: false
        };
        const obj = this.hostNewModal.userServiceMultiValues.filter(x => x.cdKeyValueId === element.id)[0];
        const index = this.hostNewModal.userServiceMultiValues.indexOf(obj);
        if (index === -1) {
          this.hostNewModal.userServiceMultiValues = this.hostNewModal.userServiceMultiValues.concat(filterItem);
        } else {
          this.hostNewModal.userServiceMultiValues[index].isDeleted = false;
        }
        // this.hostNewModal.userServiceMultiValues.splice(this.hostNewModal.userServiceMultiValues.length, 0, filterItem);
      });
    } else {
      this.hostModel.hostServiceLinkingSubCategories = this.hostModel.hostServiceLinkingSubCategories.concat(items);
      this.hostModel.hostServiceLinkingSubCategories = Array.from(new Set(this.hostModel.hostServiceLinkingSubCategories));
    }

  }

  public onLanguageDeSelectAll() {
    if (this.hostNewModal) {
      const arr = this.hostNewModal.userServiceMultiValues.map(x => Object.assign({}, x));
      arr.forEach((item, index) => {
        const hostIndex = this.hostNewModal.userServiceMultiValues.findIndex(hostItem => hostItem.cdKeyValueId === item.cdKeyValueId);
        // this.hostNewModal.userServiceMultiValues.splice(hostIndex, 1);
        this.hostNewModal.userServiceMultiValues[index].isDeleted = true;
      });
    } else {
      const arr = this.hostModel.hostServiceLinkingSubCategories.map(x => Object.assign({}, x));
      arr.forEach((item, index) => {
        if (item.svcCatgId === ServiceCategoryEnum.Languages) {
          const hostIndex = this.hostModel.hostServiceLinkingSubCategories.findIndex(hostItem => hostItem.svcCatgId === item.svcCatgId)
          this.hostModel.hostServiceLinkingSubCategories.splice(hostIndex, 1);
        }
      });
    }

  }

  checkLogin(): boolean {
    let islogin = false;
    const access_token = this.localStorageService.getAccessToken();
    if (access_token) {
      islogin = true
    }
    return islogin;
  }

  checkValidation(type) {

    let errorCount = 0;
    let error = '';
    if (this.hostModel) {
      if (this.hostModel.hostName.trim() === '') {
        if (type === 'name') {
          this.messageService.showMessage({ type: 'warning', title: 'Business Hosting', body: 'Please enter the name' });

        }
        errorCount++;
      }

      if ((this.hostModel.eMailId.trim() === '' ||
        !this.emailPattern.test(this.hostModel.eMailId))) {
        this.isEmailInvalid = true;
        if (type === 'email') {
          this.messageService.showMessage({ type: 'warning', title: 'Business Hosting', body: 'Please enter valid email-id' });

        }
        errorCount++;
      } else {
        this.isEmailInvalid = false;
      }

      if (this.hostModel.contactNo && (this.hostModel.contactNo.trim() === '' ||
        !this.phonenoPattern.test(this.hostModel.contactNo))) {
        this.isContactInvalid = true;
        if (type === 'phone') {
          this.messageService.showMessage({ type: 'warning', title: 'Business Hosting', body: 'Please enter valid phone-no' });

        }
        errorCount++;
      } else {
        this.isContactInvalid = false;
      }
    }

    if (this.hostNewModal) {
      if (this.hostNewModal.hostName) {
        if (!this.hostNewModal.hostName) {
          if (type === 'name') {
            this.messageService.showMessage({ type: 'warning', title: 'Business Hosting', body: 'Please enter the name' });

          }
          errorCount++;
        }
      }

      if ((this.hostNewModal.emailId.trim() === '' ||
        !this.emailPattern.test(this.hostNewModal.emailId))) {
        this.isEmailInvalid = true;
        if (type === 'email') {
          this.messageService.showMessage({ type: 'warning', title: 'Business Hosting', body: 'Please enter valid email-id' });

        }
        errorCount++;
      } else {
        this.isEmailInvalid = false;
      }

      if (this.hostNewModal.contactNo && (this.hostNewModal.contactNo.trim() === '' ||
        !this.phonenoPattern.test(this.hostNewModal.contactNo))) {
        this.isContactInvalid = true;
        if (type === 'phone') {
          this.messageService.showMessage({ type: 'warning', title: 'Business Hosting', body: 'Please enter valid phone-no' });

        }
        errorCount++;
      } else {
        this.isContactInvalid = false;
      }
    }


    if (this.selectedLanguages.length === 0) {
      errorCount++;
    }

    if (errorCount > 0) {
      if (type === 'all') {
        this.messageService.showMessage({ type: 'warning', title: '', body: 'Please fill the mandatory fields' });

      }

      return true;
    } else {
      return false;
    }
  }
}
