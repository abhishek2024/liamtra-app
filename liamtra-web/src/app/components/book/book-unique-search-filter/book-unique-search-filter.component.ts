import { Component, OnInit, EventEmitter, ViewContainerRef, Output, Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { IMyDpOptions } from 'mydatepicker';
import { BookService, BookUnique, ServiceSubCategoryModel } from '../shared';
import { ServiceCategoryEnum } from '../../../shared/enum/service-category-enum';
import { CityModel } from '../../shared/city.model';
import { LoaderService } from '../../../core/loader/loader.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import { ApiUrl } from '../../../api.service';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { LocalStorageService } from '../../../shared/services';

import { NativeDateAdapter, DateAdapter, MAT_DATE_FORMATS } from "@angular/material";
import { MessageService } from '../../../shared/message/messageService.service';

export class AppDateAdapter extends NativeDateAdapter {

  parse(value: any): Date | null {
    if ((typeof value === 'string') && (value.indexOf('/') > -1)) {
      const str = value.split('/');
      const year = Number(str[2]);
      const month = Number(str[1]) - 1;
      const date = Number(str[0]);
      return new Date(year, month, date);
    }
    const timestamp = typeof value === 'number' ? value : Date.parse(value);
    return isNaN(timestamp) ? null : new Date(timestamp);
  }

  format(date: Date, displayFormat: Object): string {
    // let globalValue = this.localStorageService.getGlobalInformation();
    if (displayFormat == "input") {
      let day = date.getDate();
      let month = date.getMonth() + 1;
      let year = date.getFullYear();
      return this._to2digit(month) + '/' + this._to2digit(day) + '/' + year;
    } else {
      return date.toDateString();
    }
  }

  private _to2digit(n: number) {
    return ('00' + n).slice(-2);
  }
}

export const APP_DATE_FORMATS =
  {
    parse: {
      dateInput: { month: 'short', year: 'numeric', day: 'numeric' }
    },
    display: {
      // dateInput: { month: 'short', year: 'numeric', day: 'numeric' },
      dateInput: 'input',
      monthYearLabel: { month: 'short', year: 'numeric', day: 'numeric' },
      dateA11yLabel: { year: 'numeric', month: 'long', day: 'numeric' },
      monthYearA11yLabel: { year: 'numeric', month: 'long' },
    }
  }

@Component({
  selector: 'app-book-unique-search-filter',
  templateUrl: './book-unique-search-filter.component.html',
  providers: [
    {
      provide: DateAdapter, useClass: AppDateAdapter
    },
    {
      provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS
    }
  ]
})

export class BookUniqueSearchFilterComponent implements OnInit {
  totalGuests: Array<ServiceSubCategoryModel> = [];
  bookUnique: BookUnique = new BookUnique();
  @Output() setSearchdata = new EventEmitter();
  checkInDate;
  checkOutDate;
  todayDate;
  tomorrowDate;
  cities = [];
  minDateForDatepicker;
  constructor(private router: Router,
    public http: Http,
    private messageService: MessageService,
    public route: ActivatedRoute,
    private loaderService: LoaderService,
    private bookService: BookService) {
    const todayDate = new Date();
    const datePipe = new DatePipe('en-US');
    this.todayDate = datePipe.transform(todayDate, 'MM-dd-yyyy');
    this.tomorrowDate = new Date();
    this.minDateForDatepicker = new Date(todayDate);
    this.tomorrowDate.setDate(this.tomorrowDate.getDate() + 1);
  }


  ngOnInit() {
    this.bookUnique.CheckIn = this.route.snapshot.queryParams['CheckIn'] || this.todayDate;
    this.bookUnique.CheckIn = new Date(this.bookUnique.CheckIn);
    this.bookUnique.CityName = this.route.snapshot.queryParams['CityName'] || '';
    this.bookUnique.NumberofGuestsInRoom = this.route.snapshot.queryParams['NumberofGuestsInRoom'] || '2';
    this.bookUnique.CheckOut = this.route.snapshot.queryParams['CheckOut'] || this.tomorrowDate;
    this.bookUnique.CheckOut = new Date(this.bookUnique.CheckOut);
    this.getTotalGuests(ServiceCategoryEnum.TotalAdultGuest);
  }

  valueChanged(newVal) {
    // console.log('Case 2: value is changed to ', newVal);
  }

  async getBookIndividual() {
    if (!this.bookUnique.CityName) {
      this.messageService.showMessage({ type: 'warning', title: '', body: 'Enter a valid City Name' });
      return;
    }

    if (this.bookUnique.CheckIn > this.bookUnique.CheckOut) {
      this.messageService.showMessage({ type: 'error', title: 'Filter', body: 'End date shoul not be small than start date' });
      return;
    }

    let params = {
      CityName: this.bookUnique.CityName,
      NumberofGuestsInRoom: this.bookUnique.NumberofGuestsInRoom, CheckIn: this.bookUnique.CheckIn,
      CheckOut: this.bookUnique.CheckOut
    }

    this.router.navigate(['book/unique-homes-search'], {
      queryParams: params
    })

    this.setSearchdata.emit(params);

  }
  async getTotalGuests(id) {
    // try {
    // this.loaderService.show();
    const response = await this.bookService.getServiceSubCategoryByServiceId(id);
    this.totalGuests = response.data.Result;
    // this.loaderService.hide();
  }

  observableSource = (keyword: any): Observable<any[]> => {
    const url = ApiUrl.MASTER_URI + 'City/listForDestinationSearch/' + keyword;
    if (keyword) {
      return this.http.get(url)
        .map(res => {
          const json = res.json();
          this.loaderService.hide();
          return json;
        });
    } else {
      return Observable.of([]);
    }
  }
}
