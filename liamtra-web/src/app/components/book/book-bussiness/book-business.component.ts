import { Component, OnInit } from '@angular/core';
import { BookBusinessService } from './shared/book-business.service';
import { ActivatedRoute, Router } from '@angular/router';
import { LoaderService } from '../../../core/loader';
import { CdKeyEnum } from '../../../shared/enum/cd-key.enum';

@Component({
  selector: 'app-book-bussiness',
  templateUrl: './book-business.component.html'
})
export class BookBusinessComponent implements OnInit {
  vendors = [];
  searchItem: string;
  city: string;
  experience: string;
  term: string;
  constructor(
    private bookBusinessService: BookBusinessService,
    private router: Router,
    private loaderService: LoaderService,
  ) {
  }

  ngOnInit() {
    this.getVendor();
  }



  async getVendor() {
    try {
      this.loaderService.show();
      const response = await this.bookBusinessService.getVendor(CdKeyEnum.BusinessType);
      this.vendors = response.data.Result;
      this.loaderService.hide();
    } catch (e) {
      this.loaderService.hide();
    }
  }


  businessList(data) {
    this.router.navigate(['/book/business-list'], {
      queryParams: {
        SearchItem: data.id
      }
    });
  }

}
