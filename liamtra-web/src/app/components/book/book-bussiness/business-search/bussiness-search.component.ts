import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { CoreService, CMSModel } from '../../../../core/shared';
import { CmsSystemEnum } from '../../../../shared/enum/cms-sytem-enum';
import { LoaderService } from '../../../../core/loader';
import { PaginationService } from '../../../../shared/services';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import { ApiUrl } from '../../../../api.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageService } from '../../../../shared/message/messageService.service';

@Component({
  selector: 'app-bussiness-search',
  templateUrl: './bussiness-search.component.html',
})
export class BookBussinessSearchComponent implements OnInit {
  cmsData: Array<CMSModel> = new Array<CMSModel>();
  @Input() searchItem: any;
  @Input() isExpMain: any;
  @Input() isExpList: any;
  @Input() vendorName: string;
  @Input() cityName: string;
  city: string = '';
  experience: string = '';
  src: String;
  attachmentUrl: String;
  params: string = '';
  sliderCss: string = 'bookTourSearch';
  
  public ApiUrl = ApiUrl;
  constructor(
    public http: Http,
    public router: Router,
    private loaderService: LoaderService,
    private coreService: CoreService,
    private paginationService: PaginationService,
    private messageService: MessageService) {
  }

  ngOnInit() {
    this.experience = this.vendorName;
    this.city = this.cityName;
  }

  observableSource = (keyword: any): Observable<any[]> => {
    const url = ApiUrl.MASTER_URI + 'HostBusiness/GetByBusinessName/' + keyword;
    if (keyword) {
      return this.http.get(url)
        .map(res => {
          const json = res.json();
          this.loaderService.hide();
          return json;
        });
    } else {
      return Observable.of([]);
    }
  }

  cityObservableSource = (keyword: any): Observable<any[]> => {
    const url = ApiUrl.MASTER_URI + 'City/listForDestinationSearch/' + keyword;
    if (keyword) {
      return this.http.get(url)
        .map(res => {
          const json = res.json();
          this.loaderService.hide();
          return json;
        });
    } else {
      return Observable.of([]);
    }
  }

  getBookbusiness() {
    if (this.experience || this.city) {
      this.router.navigate(['/book/business-list'], {
        queryParams: {
          BusinessName: this.experience,
          CityName: this.city,
          SearchItem: this.searchItem
        }
      });
    } else {
      this.messageService.showMessage({ type: 'error', title: 'Business', body: 'Please Select Business Name Or City' });

    }
  }


}
