import { Http, Headers, RequestOptions, BaseRequestOptions, Response } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { ApiUrl } from '../../../../api.service';
import {
    ObjectResponseModel,
    PostObjectResponseModel,
    ArrayResponseModel,
    AsyncArrayPromiseHandler,
    AsyncObjectPromiseHandler,
    BaseDataModel
} from '../../../../shared/models/base-data.model';
import { CityModel } from '../../../shared/city.model';
@Injectable()
export class BookBusinessService {

    constructor(private http: Http) { }


    getVendor(params): Promise<ArrayResponseModel<any>> {
        const promise = this.http.get(ApiUrl.HOST_URI + 'CdKeyValue/GetByCDKeyID/' + params);
        return new AsyncArrayPromiseHandler<any>(promise.toPromise());
    }

    getBusinessList(params): Promise<ObjectResponseModel<BaseDataModel<any>>> {
        const promise = this.http.get(ApiUrl.HOST_URI + 'HostBusiness/GetForSearching', { params: params });
        return new AsyncObjectPromiseHandler<BaseDataModel<any>>(promise.toPromise());
    }

    getBusinessIndividual(id, userId): Promise<ObjectResponseModel<any>> {
        const promise = this.http.get(ApiUrl.HOST_URI + 'HostBusiness/GetById/' + id + '/' + userId);
        return new AsyncObjectPromiseHandler<any>(promise.toPromise());
    }

    getBusinessListType(id): Promise<ArrayResponseModel<any>> {
        const promise = this.http.get(ApiUrl.HOST_URI + 'CdKeyValue/GetByCDKeyID/' + id);
        return new AsyncArrayPromiseHandler<any>(promise.toPromise());
    }

    getBusinessFilteredList(params): Promise<ObjectResponseModel<BaseDataModel<any>>> {
        const promise = this.http.get(ApiUrl.HOST_URI + 'HostBusiness/GetForSearching', { params: params });
        return new AsyncObjectPromiseHandler<BaseDataModel<any>>(promise.toPromise());
    }

}
