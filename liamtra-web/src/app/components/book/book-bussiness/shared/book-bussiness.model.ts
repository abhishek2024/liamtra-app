export class BusinessListDetailModel {
  id: number = 0;
  userId: number = 0;
  hostName: string;
  CityName: string;
  BusinessName: string;
  VendorType: any;
  BusinessId: any;
}

export class BookBusinessModel {
  address1: string;
  address2: string;
  cityId: number;
  contactNo: string;
  vendorName: string;
  totalReviews: number;
  averageReview: number;
  description: string;
  emailId: string;
  businessDescription: string;
  businessName: string;
  hostName: string;
  isFavorite: boolean = false;
  id: number;
  landmark: string;
  latitude: string;
  longitude: string;
  serviceName: string;
  serviceStatusId: number;
  serviceStatusName: string;
  userServiceId: number;
  zipCode: string;
  images: Array<any> = [];
  videos: Array<any> = [];
  cityName: any;
  userServiceAsset: Array<TourAttachment> = new Array<TourAttachment>();
  userServiceMultiValues: Array<UserServiceMultiValue> = new Array<UserServiceMultiValue>();
  userServiceReview: Array<any> = [];
}

export class UserServiceMultiValue {
  id: number = 0;
  userServiceId: number = 0;
  cdKeyValueId: number = 0;
  cdKeyValue: string = '';
  cdKeyId: number = 0;
}



export class TourAttachment {
  id: number = 0;
  userServiceId: number = 0;
  assetTypeId: number = 0;
  fileName: string = '';
  isActive: boolean = false;
  isCoverPhoto: boolean = false;
  attachmentDataBase64: string = '';
  attachmentData: string = '';
  attachmentUrl: string = '';
}


export class BusinessTypeModel {
  cdKeyId: number;
  createdBy: number;
  creationDate: string;
  displayName: string;
  displayOrder: number;
  id: number;
  isActive: boolean;
  modificationDate: string;
  modifiedBy: number;
  value: number;
  isChecked?: boolean = false;
}

export class BusinessFilterModel {
  VendorType: any;
}
