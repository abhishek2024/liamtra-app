import { Component, OnInit } from '@angular/core';
import { BookBusinessService } from '../shared/book-business.service';
import { ActivatedRoute, Router } from '@angular/router';
import { BusinessListDetailModel, BusinessTypeModel, BusinessFilterModel, BookBusinessModel } from '../shared/book-bussiness.model';
import { PaginationService } from '../../../../shared/services';
import { LoaderService } from '../../../../core/loader';
import { AssertTypeEnum, CdKeyEnum } from '../../../../shared/enum/cd-key.enum';
import { ApiUrl } from '../../../../api.service';

@Component({
  selector: 'app-book-business-list',
  templateUrl: './business-list.component.html'
})
export class BookBusinessListComponent implements OnInit {
  businessListType: Array<BusinessTypeModel> = [];
  businessFilterdata: BusinessFilterModel = new BusinessFilterModel();
  filterTypeIds: string = '';
  images = [];
  businessList: Array<BookBusinessModel> = Array<BookBusinessModel>();
  sliderCss: string = 'bookBusinessSearch';
  locations = [];
  avgReviews = 3;
  locationCss: string = 'locationMapBooking';
  city: string;
  isMarkerDraggable: boolean = false;
  businessListDetail: BusinessListDetailModel = new BusinessListDetailModel();
  total: number = 0;
  page: number = 1;
  limit: number = 9;


  constructor(
    private bookBusinessService: BookBusinessService,
    private paginationService: PaginationService,
    private loaderService: LoaderService,
    public router: Router,
    public route: ActivatedRoute,
  ) {
  }

  ngOnInit() {
    this.route.queryParams.subscribe((param) => {
      this.businessListDetail.BusinessName = param['BusinessName'];
      this.businessListDetail.CityName = param['CityName'];
      this.businessListDetail.BusinessId = +param['SearchItem'];
      this.getBusinessList();
    });
    this.getBusinessListType();
  }

  async getBusinessList() {
    try {
      this.loaderService.show();
      if (this.page === 1) {
        this.paginationConfig();
      }
      this.businessListDetail.userId = +localStorage.getItem('userId') !== 0 ? +localStorage.getItem('userId') : 0;
      this.paginationService.setFilterValues(this.businessListDetail);
      this.paginationService.pageSize = this.limit;
      const response = await this.bookBusinessService.getBusinessList(this.paginationService.getParams());
      this.businessList = response.data.Result.data;
      this.total = response.data.Result.totalRecords;
      this.reArrangeData();
      this.loaderService.hide();
    } catch (e) {
      this.loaderService.hide();
    }
  }



  async getBusinessListType() {
    try {
      const response = await this.bookBusinessService.getBusinessListType(CdKeyEnum.BusinessType);
      this.businessListType = response.data.Result;

    } catch (e) {
      this.loaderService.hide();
    }
  }


  businessDetail(id) {
    this.router.navigate(['book/business-detail'], {
      queryParams: {
        CityName: this.businessListDetail.CityName,
        id: id
      }
    });
  }

  async filteredBusiness(data) {
    try {
      this.loaderService.show();
      data.isChecked = !data.isChecked;
      if (data.isChecked) {
        this.filterTypeIds = this.filterTypeIds !== '' ? this.filterTypeIds + ',' + data.id : data.id.toString();
      } else {
        const arr = this.filterTypeIds.split(',');
        const index = arr.findIndex(i => i === data.id.toString());
        arr.splice(index, 1);
        this.filterTypeIds = '';
        arr.forEach((id, i) => {
          if (i === 0) {
            this.filterTypeIds += id;
          } else {
            this.filterTypeIds += ',' + id;
          }
        });
      }
      this.businessListDetail.VendorType = this.filterTypeIds;
      if (this.page === 1) {
        this.paginationConfig();
      }
      this.paginationService.setFilterValues(this.businessListDetail);
      const response = await this.bookBusinessService.getBusinessFilteredList(this.paginationService.getParams());
      this.businessList = response.data.Result.data;
      this.total = response.data.Result.totalRecords;
      this.reArrangeData();
      this.loaderService.hide();
    } catch (e) {
      this.loaderService.hide();
    }

  }

  reArrangeData() {
    this.locations = [];
    this.businessList.forEach(experiencelisting => {
      this.locations.splice(this.locations.length, -1,
        { latitude: +(experiencelisting.latitude), longitude: +(experiencelisting.longitude) });
      experiencelisting.images = [];
      experiencelisting.videos = [];
      experiencelisting.userServiceAsset.forEach(asset => {
        if (asset.assetTypeId === AssertTypeEnum.Image) {
          const obj = {
            imgSrc: ApiUrl.SRC_URI + 'attachments/' + asset.fileName,
            sType: 'img'
          };
          experiencelisting.images.splice(experiencelisting.images.length, 0, obj);
        } else if (asset.assetTypeId === AssertTypeEnum.Video) {
          const obj = {
            videoSrc: ApiUrl.SRC_URI + asset.attachmentUrl
          };
          experiencelisting.videos.splice(experiencelisting.videos.length, 0, obj);
        }
      });
    });
    if (this.locations.length === 0) {
      this.locations = [{ 'latitude': 28.89813800000001, 'longitude': 76.56141400000001 }];
    }

    this.loaderService.hide();
  }

  goToPage(n: number): void {
    this.page = n;
    const obj = {
      page: this.page,
      rows: this.limit
    };
    this.paginationService.setPageChange(obj);
    this.getBusinessList();
  }

  onNext(): void {
    this.page++;
    this.paginationConfig();
    this.getBusinessList();
  }

  onPrev(): void {
    this.page--;
    this.paginationConfig();
    this.getBusinessList();
  }

  paginationConfig() {
    const obj = {
      page: this.page,
      rows: this.limit
    };
    this.paginationService.setPageChange(obj);
  }

}
