import { Component, OnInit, ViewChild } from '@angular/core';
import { BookService, FavoriteModel, ReportModel, LanguageModel } from '../../shared';
import { LoaderService } from '../../../../core/loader/loader.service';
import { ActivatedRoute, Router } from '@angular/router';
import { BookBusinessService } from '../shared/book-business.service';
import { CdKeyEnum, UserServiceReportEnum } from '../../../../shared/enum/cd-key.enum';
import { BookBusinessModel, BusinessListDetailModel } from '../shared/book-bussiness.model';
import { AssertTypeEnum } from '../../../../shared/enum/cd-key.enum';
import { ApiUrl } from '../../../../api.service';
import { MessageService } from '../../../../shared/message/messageService.service';
import { PaginationService, LocalStorageService } from '../../../../shared/services';
import { BookUniqueHomeReportListing } from '../../../../core/dialog-component/book-unique-home-report/book-unique-home-report.component'
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
    selector: 'app-book-business-detail',
    templateUrl: './business-detail.component.html'
})

export class BookBusinessDetailComponent implements OnInit {
    businessDetail: BookBusinessModel = new BookBusinessModel();
    businessListDetail: BusinessListDetailModel = new BusinessListDetailModel();
    cityName: string;
    locationCss: string = 'bookTourMap';
    favoriteModel: FavoriteModel = new FavoriteModel();
    reportListing: ReportModel = new ReportModel();
    languages: Array<LanguageModel> = [];
    modal: boolean = false;
    modalStatus: string = '';
    modalType: number;
    sliderCss: string = 'bookUniqueHomeDetail';
    reviewModal: boolean = false;
    isMarkerDraggable = false;
    @ViewChild('profile') profile;
    @ViewChild('reviews') reviews;

    constructor(private loaderService: LoaderService,
        private paginationService: PaginationService,
        private localStorageService: LocalStorageService,
        private messageService: MessageService,
        private router: Router,
        public route: ActivatedRoute,
        public dialog: MatDialog,
        private bookService: BookService,
        private bookExperienceService: BookBusinessService) {

    }

    ngOnInit() {
        this.route.queryParams.subscribe((param) => {
            this.businessListDetail.id = +param['id'];
            this.cityName = param['CityName'];
            this.getBookExperienceIndividual();
        });

    }

    async getBookExperienceIndividual() {
        try {
            this.loaderService.show();
            this.businessListDetail.userId = +localStorage.getItem('userId') !== 0 ? +localStorage.getItem('userId') : 0;
            // this.paginationService.setFilterValues(this.businessListDetail);
            const response = await this.bookExperienceService.getBusinessIndividual(this.businessListDetail.id,
                this.businessListDetail.userId);
            this.businessDetail = response.data.Result;
            this.businessDetail.userServiceMultiValues.forEach(item => {
                if (item.cdKeyId === CdKeyEnum.Languages) {
                    this.languages.push(item);
                }
            });
            this.cityName = this.businessDetail.cityName;
            this.businessDetail.images = [];
            this.businessDetail.videos = [];
            this.businessDetail.userServiceAsset.forEach(asset => {
                if (asset.assetTypeId === AssertTypeEnum.Image) {
                    const obj = {
                        imgSrc: ApiUrl.SRC_URI + 'attachments/' + asset.fileName,
                        sType: 'img'
                    };
                    this.businessDetail.images.splice(this.businessDetail.images.length, 0, obj);
                } else if (asset.assetTypeId === AssertTypeEnum.Video) {
                    const obj = {
                        videoSrc: ApiUrl.SRC_URI + asset.attachmentUrl
                    };
                    this.businessDetail.videos.splice(this.businessDetail.videos.length, 0, obj);
                }
            });
            this.loaderService.hide();
        } catch (e) {
            this.loaderService.hide();
        }

    }

    opemModal(modalStatus) {
        this.modalStatus = modalStatus;
        this.modal = true;
    }

    async checkUserLogin() {
        try {
            const access_token = this.localStorageService.getAccessToken();
            if (access_token) {
                return true;
            } else {
                this.opemModal('signIn');
                return false;
            }
        } catch (e) { }
    }

    async openDialog() {
        const isLogin: boolean = await this.checkUserLogin();
        if (isLogin) {
            const dialogRef = this.dialog.open(BookUniqueHomeReportListing, {
                width: '1100px',
                data: { name: 'Business Report Modal' }
            });
            dialogRef.afterClosed().subscribe(async result => {
                if (result) {
                    this.reportListing.userServiceId = this.businessDetail.userServiceId;
                    this.reportListing.remarks = result;
                    this.reportListing.cdKeyValueId = UserServiceReportEnum.Requested;
                    try {
                        this.loaderService.show();
                        const response = await this.bookService.reportIssue(this.reportListing);
                        if (response.data.Result) {
                            this.messageService.showMessage({ type: 'success', title: '', body: 'Reported Successfully' });
                        }
                        this.loaderService.hide();
                    } catch (e) {
                        this.loaderService.hide();
                    }
                }
            });
        }
    }


    async bookFavorite() {
        try {
            const access_token = this.localStorageService.getAccessToken();
            if (access_token) {
                this.favoriteModel.isFavorite = !this.favoriteModel.isFavorite;
                this.favoriteModel.userServiceId = this.businessDetail.userServiceId;
                this.loaderService.show();
                const response = await this.bookService.bookFavorite(this.favoriteModel);
                this.getBookExperienceIndividual();
            } else {
                this.modalStatus = 'signIn';
                this.modal = true;
                this.messageService.showMessage({ type: 'error', title: 'Business', body: 'Please login before make it favorite' });
            }
        } catch (e) {
            this.loaderService.hide();
        }
    }

    modalPopupEvent(result) {
        this.modal = result;
    }

    closePopupEvent(result) {
        this.reviewModal = result;
        this.getBookExperienceIndividual();
    }

    openReviewModal() {
        const access_token = this.localStorageService.getAccessToken();
        if (access_token) {
            this.reviewModal = true;
        } else {
            this.modalStatus = 'signIn';
            this.modal = true;
            this.messageService.showMessage({ type: 'error', title: 'Business', body: 'Please login before review it' });
        }
    }


    scroll(el) {
        if (el === 'reviews') {
            this.reviews.nativeElement.scrollIntoView({ behavior: "smooth" });
        }
        if (el === 'profile') {
            this.profile.nativeElement.scrollIntoView({ behavior: "smooth" });
        }
    }

}
