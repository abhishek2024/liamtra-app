import { Component, OnInit } from '@angular/core';
import { CoreService, CMSModel } from '../../../../core/shared';
import { CmsSystemEnum } from '../../../../shared/enum/cms-sytem-enum';
import { LoaderService } from '../../../../core/loader';
import { ApiUrl } from '../../../../api.service';
import { BookTourService, BookTour } from '../shared/';
import { BookTourFilterModel } from '..';
import { PaginationService, LocalStorageService } from '../../../../shared/services';
import { ActivatedRoute, Router } from '@angular/router';
import { AssertTypeEnum, UserServiceReportEnum } from '../../../../shared/enum/cd-key.enum';
import { ModalType } from '../../../../shared/enum/service-category-enum';
import { BookService, FavoriteModel, ReportModel, LanguageModel } from '../../shared';
import { MessageService } from '../../../../shared/message/messageService.service';
import { BookUniqueHomeReportListing } from '../../../../core/dialog-component/book-unique-home-report/book-unique-home-report.component'
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { CdKeyEnum } from '../../../../shared/enum/cd-key.enum';

@Component({
  selector: 'app-tour-detail',
  templateUrl: './tour-detail.component.html',
})
export class BookTourDetailComponent implements OnInit {
  bookTour: BookTour = new BookTour();
  sliderCss: string = 'bookUniqueHomeDetail';
  modal: boolean = false;
  reviewModal: boolean = false;
  modalStatus: string = '';
  reportListing: ReportModel = new ReportModel();
  favoriteModel: FavoriteModel = new FavoriteModel();
  languages: Array<LanguageModel> = [];

  constructor(
    private loaderService: LoaderService,
    private coreService: CoreService,
    private localStorageService: LocalStorageService,
    private router: Router,
    public route: ActivatedRoute,
    private bookService: BookService,
    public dialog: MatDialog,
    private bookTourService: BookTourService,
    private messageService: MessageService,
    private paginationService: PaginationService) {
    this.bookTour.id = parseInt(this.route.snapshot.params.tourId);
  }

  ngOnInit() {
    this.getTourIndividual();
  }

  async getTourIndividual() {
    try {
      this.loaderService.show();
      const userId = +localStorage.getItem('userId') !== 0 ? +localStorage.getItem('userId') : 0;
      const response = await this.bookTourService.getTourIndividual(this.bookTour.id, userId);
      this.bookTour = response.data.Result;
      this.bookTour.userServiceMultiValues.forEach(item => {
        if (item.cdKeyId === CdKeyEnum.Languages) {
          this.languages.push(item);
        }
      });
      this.bookTour.images = [];
      this.bookTour.videos = [];
      this.bookTour.userServiceAssets.forEach(asset => {
        if (asset.assetTypeId === AssertTypeEnum.Image) {
          const obj = {
            imgSrc: ApiUrl.SRC_URI + 'attachments/' + asset.fileName,
            sType: 'img'
          };
          this.bookTour.images.splice(this.bookTour.images.length, 0, obj);
        } else if (asset.assetTypeId === AssertTypeEnum.Video) {
          const obj = {
            videoSrc: ApiUrl.SRC_URI + asset.attachmentUrl
          };
          this.bookTour.videos.splice(this.bookTour.videos.length, 0, obj);
        }
      });
      this.loaderService.hide();
    } catch (e) {
      this.loaderService.hide();
    }
  }

  routeHomeSearch() {
    let date = new Date(this.bookTour.startDate);
    let month = date.getMonth() + 1;
    this.router.navigate(['book/tour-listing'], {
      queryParams: {
        destination: this.bookTour.destination,
        month: month
      }
    });
  }

  routeCheckout() {
    const access_token = this.localStorageService.getAccessToken();
    if (access_token) {
      this.router.navigate(['book/tour-checkout'], {
        queryParams: {
          id: this.bookTour.id,
          stayDuration: this.bookTour.stayDuration,
          totalAdults: this.bookTour.totalAdults,
          totalRooms: this.bookTour.totalRooms,
          packagePrice: this.bookTour.packagePrice,
          startDate: this.bookTour.startDate,
          endDate: this.bookTour.endDate,
          gst: this.bookTour.gst,
          totalNights: this.bookTour.totalNights,
          sellerName: this.bookTour.sellerName,
          totalDays: this.bookTour.totalDays,
          title: this.bookTour.title
        }
      });
    } else {
      this.messageService.showMessage({ type: 'error', title: 'Business', body: 'Please login before book this tour' });
      this.modalStatus = 'signIn';
      this.modal = true;
    }
  }

  modalPopupEvent(result) {
    this.modal = result;
  }

  closePopupEvent(result) {
    this.reviewModal = result;
    this.getTourIndividual();
  }

  openReviewModal() {
    const access_token = this.localStorageService.getAccessToken();
    if (access_token) {
      this.reviewModal = true;
    } else {
      this.modalStatus = 'signIn';
      this.modal = true;
      this.messageService.showMessage({ type: 'error', title: 'Business', body: 'Please login before review it' });
    }
  }

  opemModal(modalStatus) {
    this.modalStatus = modalStatus;
    this.modal = true;
  }

  async checkUserLogin() {
    try {
      const access_token = this.localStorageService.getAccessToken();
      if (access_token) {
        return true;
      } else {
        this.opemModal('signIn');
        return false;
      }
    } catch (e) { }
  }

  async openDialog() {
    const isLogin: boolean = await this.checkUserLogin();
    if (isLogin) {
      const dialogRef = this.dialog.open(BookUniqueHomeReportListing, {
        width: '1100px'
      });
      dialogRef.afterClosed().subscribe(async result => {
        if (result) {
          this.reportListing.userServiceId = this.bookTour.userServiceId;
          this.reportListing.remarks = result;
          this.reportListing.cdKeyValueId = UserServiceReportEnum.Requested;
          try {
            this.loaderService.show();
            const response = await this.bookService.reportIssue(this.reportListing);
            if (response.data.Result) {
              this.messageService.showMessage({ type: 'success', title: '', body: 'Reported Successfully' });
            }
            this.loaderService.hide();
          } catch (e) {
            this.loaderService.hide();
          }
        }
      });
    }
  }


  async bookFavorite() {
    try {
      const access_token = this.localStorageService.getAccessToken();
      if (access_token) {
        this.favoriteModel.isFavorite = !this.favoriteModel.isFavorite;
        this.favoriteModel.userServiceId = this.bookTour.userServiceId;
        this.loaderService.show();

        let response = await this.bookService.bookFavorite(this.favoriteModel);
        // this.loaderService.hide();
        this.getTourIndividual();
      } else {
        this.modalStatus = 'signIn';
        this.modal = true;
        this.messageService.showMessage({ type: 'error', title: 'Business', body: 'Please login before make it favorite' });
      }
    } catch (e) {
      this.loaderService.hide();
    }
  }
}
