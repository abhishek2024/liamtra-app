import { Component, OnInit, EventEmitter, Output, Pipe, PipeTransform, Input, OnChanges } from '@angular/core';
import { Router } from '@angular/router';
import { BookService, BookUnique, ServiceSubCategoryModel } from '../../shared';
import { ServiceCategoryEnum } from '../../../../shared/enum/service-category-enum';
import { LoaderService } from '../../../../core/loader/loader.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import { ApiUrl } from '../../../../api.service';
import { Http } from '@angular/http';
import { BookTourFilterModel } from '../index';
import { CustomDDO } from '../../../shared/custom-ddo.model';
import { MasterService } from '../../../shared/service/master.service';
import { MessageService } from '../../../../shared/message/messageService.service';

@Component({
  selector: 'app-book-tour-search-filter',
  templateUrl: './tour-search.component.html'
})

export class BookTourSearchFilterComponent implements OnInit, OnChanges {

  @Input() destination: string = '';
  @Input() month: number = 0;
  bookTourFilterModel: BookTourFilterModel = new BookTourFilterModel();
  months: Array<CustomDDO> = new Array<CustomDDO>();
  constructor(private router: Router,
    public http: Http,
    private messageService: MessageService,
    private loaderService: LoaderService,
    private masterService: MasterService) {
  }

  ngOnInit() {
    this.getMonths();
  }

  ngOnChanges() {
    this.bookTourFilterModel.destination = this.destination;
    this.bookTourFilterModel.month = +this.month;
  }

  async getBookTours() {
    if (this.bookTourFilterModel.month === 0 || this.bookTourFilterModel.destination.trim() === "") {
      this.messageService.showMessage({ type: 'error', title: 'Business', body: 'Destination & Month should be selected' });
      return;
    }
    this.router.navigate(['book/tour-listing'], {
      queryParams: {
        destination: this.bookTourFilterModel.destination,
        month: this.bookTourFilterModel.month
      }
    });
  }

  getMonths() {
    const response = this.masterService.getMonths();
    this.months = response;
  }

  observableSource = (keyword: any): Observable<any[]> => {
    const url = ApiUrl.MASTER_URI + 'City/listForDestinationSearch/' + keyword;
    if (keyword) {
      return this.http.get(url)
        .map(res => {
          const json = res.json();
          this.loaderService.hide();
          return json;
        });
    } else {
      return Observable.of([]);
    }
  }
}
