import { Component, OnInit } from '@angular/core';
import { LoaderService } from '../../../../core/loader';
import { BookTourFilterModel, BookTour, BookTourService } from '../index';
import { PaginationService } from '../../../../shared/services';
import { ActivatedRoute, Router } from '@angular/router';
import { AssertTypeEnum } from '../../../../shared/enum/cd-key.enum';
import { ApiUrl } from '../../../../api.service';

@Component({
  selector: 'app-tour-listing',
  templateUrl: './tour-listing.component.html',
})

export class BookTourListingComponent implements OnInit {
  attachmentUrl: String;
  bookTourFilterModel: BookTourFilterModel = new BookTourFilterModel();
  bookTourListing: Array<BookTour> = new Array<BookTour>();
  sliderCss: string = 'bookTourSearch';

  constructor(
    private loaderService: LoaderService,
    public route: ActivatedRoute,
    private bookTourService: BookTourService,
    private paginationService: PaginationService) {
    this.route.queryParams.subscribe((param) => {
      this.bookTourFilterModel.destination = param['destination'];
      this.bookTourFilterModel.month = param['month'];
      this.getTours();
    });
  }

  ngOnInit() {
    // this.getTours();
  }

  async getTours() {
    try {
      this.loaderService.show();
      this.bookTourFilterModel.userId = +localStorage.getItem('userId') !== 0 ? +localStorage.getItem('userId') : 0;
      this.paginationService.setFilterValues(this.bookTourFilterModel);
      let response = await this.bookTourService.getTours(this.paginationService.getParams());
      this.bookTourListing = response.data.Result.data;
      this.bookTourListing.forEach(tourlisting => {
        tourlisting.images = [];
        tourlisting.videos = [];
        tourlisting.userServiceAssets.forEach(asset => {
          if (asset.assetTypeId === AssertTypeEnum.Image) {
            const obj = {
              imgSrc: ApiUrl.SRC_URI + 'attachments/' + asset.fileName,
              sType: 'img'
            }
            tourlisting.images.splice(tourlisting.images.length, 0, obj);
          } else if (asset.assetTypeId === AssertTypeEnum.Video) {
            let obj = {
              videoSrc: ApiUrl.SRC_URI + asset.attachmentUrl
            }
            tourlisting.videos.splice(tourlisting.videos.length, 0, obj);
          }
        });
      });
      this.loaderService.hide();
    } catch (e) {
      this.loaderService.hide();
    }
  }
}
