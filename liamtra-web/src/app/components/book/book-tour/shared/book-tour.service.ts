import { Http, Headers, RequestOptions, BaseRequestOptions, Response } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { ApiUrl } from '../../../../api.service';
import {
    ObjectResponseModel,
    PostObjectResponseModel,
    ArrayResponseModel,
    AsyncArrayPromiseHandler,
    AsyncObjectPromiseHandler,
    BaseDataModel
} from '../../../../shared/models/base-data.model';
import { CityModel } from '../../../shared/city.model';
import { BookTour } from '../shared/book-tour.model';
// import { BookTourSearchData } from '../../book-vendor/shared/book-tour.model';

@Injectable()
export class BookTourService {

    constructor(private http: Http) { }

    getTourIndividual(bookTourId, userId): Promise<ObjectResponseModel<BookTour>> {
        const promise = this.http.get(ApiUrl.BOOK_URI + 'TourBooking/getForBooking/' + bookTourId + '/' + userId);
        return new AsyncObjectPromiseHandler<BookTour>(promise.toPromise());
    }

    getTours(params): Promise<ObjectResponseModel<BaseDataModel<BookTour>>> {
        const promise = this.http.get(ApiUrl.BOOK_URI + 'Tour/listForBooking', { params: params });
        return new AsyncObjectPromiseHandler<BaseDataModel<BookTour>>(promise.toPromise());
    }

    saveBookTour(data): Promise<ObjectResponseModel<any>> {
        let promise = null;
        promise = this.http.post(ApiUrl.HOST_URI + 'TourBooking', data);
        return new AsyncObjectPromiseHandler<any>(promise.toPromise());
    }
}
