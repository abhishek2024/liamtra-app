
export class BookTourFilterModel {
    userId: number = 0;
    tourId: number = 0;
    destination: string = '';
    month: number = 0;
}

export class BookTour {
    id: number = 0;
    userServiceId: number = 0;
    title: string = '';
    sellerId: number = 0;
    sellerName: string = '';
    startDate: Date = new Date();
    endDate: Date = new Date();
    source: string = '';
    destination: string = '';
    totalDays: number;
    totalNights: number;
    totalSeats: number;
    totalRooms: number;
    totalAdults: number;
    stayDuration: number;
    packagePrice: number;
    gst: number;
    isFavorite: boolean = false;
    isTermAccepted: boolean = false
    tourStayPlans: Array<TourStayPlan> = new Array<TourStayPlan>();
    tourContents: Array<TourContent> = new Array<TourContent>();
    userServiceAssets: Array<TourAttachment> = new Array<TourAttachment>();
    userServiceMultiValues: Array<UserServiceMultiValue> = new Array<UserServiceMultiValue>();
    userServiceReviews: Array<UserServiceReview> = Array<UserServiceReview>();
    canReview: boolean = false;
    avgReview: number = 0;
    images: Array<any> = [];
    videos: Array<any> = [];
}


export class UserServiceMultiValue {
    id: number = 0;
    userServiceId: number = 0;
    cdKeyValueId: number = 0;
    cdKeyValue: string = '';
    cdKeyId: number = 0;
}

export class TourStayPlan {
    id: number = 0;
    tourId: number = 0;
    cityName: string = '';
    totalNights: string = '';
}

export class TourContent {
    id: number = 0;
    tourId: number = 0;
    tourContentFieldId: number = 0;
    contentValue: any;
    contentName: string = '';
}

export class TourAttachment {
    id: number = 0;
    userServiceId: number = 0;
    assetTypeId: number = 0;
    fileName: string = '';
    isActive: boolean = false;
    isCoverPhoto: boolean = false;
    attachmentDataBase64: string = '';
    attachmentData: string = '';
    attachmentUrl: string = '';

}

export class TourDetailModel {
    id: number = 0;
    tourId: number = 0;
    startDate: Date = new Date();
    endDate: Date = new Date();
    packagePrice: number = 0;
    gst: number = 0;
    totalDays: number = 0;
    totalNights: number = 0;
    totalRooms: number = 0;
    totalAdults: number = 0;
    stayDuration: number = 0;
    guestName: string = '';
    emailAddress: string = '';
    mobileNo: string = '';
    sellerName: string = '';
    title: string = '';
    introduction: string = '';
}


export class BookExperienceModel {
    averageReview: number;
    totalReview: number;
    address1: string;
    address2: string;
    cityId: number;
    contactNo: string;
    description: string;
    emailId: string;
    experienceDescription: string;
    averageReviews: number = 0;
    experienceName: string;
    hostName: string;
    isFavorite: boolean = false;
    id: number;
    landmark: string;
    latitude: string;
    longitude: string;
    serviceName: string;
    serviceStatusId: number;
    serviceStatusName: string;
    userServiceId: number;
    zipCode: string;
    images: any;
    videos: any;
    cityName: any;
    userServiceAsset: Array<TourAttachment> = new Array<TourAttachment>();
    userServiceMultiValues: Array<UserServiceMultiValue> = new Array<UserServiceMultiValue>();
    userServiceReview: Array<any> = [];
}

export class ExperienceByIdModel {
    id: any;
}

export class UserServiceReview {
    id: number = 0;
    userServiceId: number = 0;
    ratingValue: number = 0;
    remarks: string = '';
    userName: string = '';
    creationDate: Date = new Date();
}

export class ExpFilterModel {
    HostName: string;
    ExperienceName: string;
    CityName: string;
    Name: string;
}
