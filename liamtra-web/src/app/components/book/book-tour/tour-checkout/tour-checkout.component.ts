import { Component, OnInit, ViewContainerRef, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LoaderService } from '../../../../core/loader/loader.service';
import { LocalStorageService } from '../../../../shared/services/local-storage.service';
import { BookTour, TourDetailModel, BookTourService } from '../shared';
import { MessageService } from '../../../../shared/message/messageService.service';

@Component({
  selector: 'app-book-tour-checkout',
  templateUrl: './tour-checkout.component.html'
})

export class BookTourCheckoutComponent implements OnInit {

  public termAndConditionmodal: boolean = false;
  isTermAccepted: boolean = false;
  tourDetailModel: TourDetailModel = new TourDetailModel();
  emailPattern = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  phonenoPattern = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
  modal: boolean = false;
  modalStatus: string = '';

  constructor(private loaderService: LoaderService,
    public bookTourService: BookTourService,
    public localStorageService: LocalStorageService,
    public router: Router,
    public route: ActivatedRoute,
    private messageService: MessageService) {
  }

  ngOnInit() {
    this.tourDetailModel.tourId = this.route.snapshot.queryParams['id'];
    this.tourDetailModel.stayDuration = this.route.snapshot.queryParams['stayDuration'];
    this.tourDetailModel.totalAdults = this.route.snapshot.queryParams['totalAdults'];
    this.tourDetailModel.totalRooms = this.route.snapshot.queryParams['totalRooms'];
    this.tourDetailModel.packagePrice = +this.route.snapshot.queryParams['packagePrice'];
    this.tourDetailModel.startDate = this.route.snapshot.queryParams['startDate'];
    this.tourDetailModel.endDate = this.route.snapshot.queryParams['endDate'];
    this.tourDetailModel.gst = +this.route.snapshot.queryParams['gst'];
    this.tourDetailModel.totalNights = this.route.snapshot.queryParams['totalNights'];
    this.tourDetailModel.totalDays = this.route.snapshot.queryParams['totalDays'];
    this.tourDetailModel.sellerName = this.route.snapshot.queryParams['sellerName'];
    this.tourDetailModel.title = this.route.snapshot.queryParams['title'];
  }

  getTermsAndConditions() {
    this.termAndConditionmodal = true;
  }

  termsAndConditionsPopupEvent(event) {
    this.termAndConditionmodal = false;
  }

  async  bookTour(type) {
    let isUserLoggedIn = this.checkUserLogin();
    try {
      let error = false;
      let errorMessage = 'Please fill the required fields';
      if (this.tourDetailModel.guestName.trim() === '' || this.tourDetailModel.mobileNo.trim() === '') {
        error = true;
      }
      if ((this.tourDetailModel.emailAddress.trim() === '' ||
        !this.emailPattern.test(this.tourDetailModel.emailAddress))) {
        if (type === 'email') {
          errorMessage = 'Please enter valid email-id';
          this.messageService.showMessage({ type: 'warning', title: '', body: errorMessage });

        }
        error = true;
      }

      if (this.tourDetailModel.mobileNo && (this.tourDetailModel.mobileNo.trim() === '' ||
        !this.phonenoPattern.test(this.tourDetailModel.mobileNo))) {
        error = true;
        if (type === 'mobileNo') {
          errorMessage = 'Please enter valid phone-no';
          this.messageService.showMessage({ type: 'warning', title: '', body: errorMessage });

        }
      }

      if (error) {
        if (type === 'all') {
          this.messageService.showMessage({ type: 'warning', title: '', body: errorMessage });

        }
        return;
      }
      if (type === 'all' && isUserLoggedIn) {
        this.loaderService.show();
        let response = await this.bookTourService.saveBookTour(this.tourDetailModel);
        this.router.navigate(['book/thank-you/' + response.data.Result.genValue]);
        this.loaderService.hide();
      }
    } catch (e) {
      this.loaderService.hide();
    }
  }

  checkUserLogin() {
    try {
      const access_token = this.localStorageService.getAccessToken();
      if (access_token) {
        return true;
      } else {
        this.modalStatus = 'signIn';
        this.modal = true;
        return false;
      }
    } catch (e) { }
  }

  modalPopupEvent(result) {
    this.modal = result;
  }
}
