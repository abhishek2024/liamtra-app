import { Http, Headers, RequestOptions, BaseRequestOptions, Response } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { ApiUrl } from '../../../../api.service';
import {
    ObjectResponseModel,
    PostObjectResponseModel,
    ArrayResponseModel,
    AsyncArrayPromiseHandler,
    AsyncObjectPromiseHandler
} from '../../../../shared/models/base-data.model';
import { BookUniqueHomeDetailModel, ReportListingModel, ReviewModal } from './book-unique-home.model';

@Injectable()
export class BookUniqueHomeService {

    constructor(private http: Http) { }

    getBookUniqueDetail(params): Promise<ObjectResponseModel<BookUniqueHomeDetailModel>> {
        const promise = this.http
            .get(ApiUrl.HOST_URI + 'BookService/GetUniqueHomeIndividuals/', { params: params });
        return new AsyncObjectPromiseHandler<BookUniqueHomeDetailModel>(promise.toPromise());
    }

    sendContactHostMessage(params): Promise<ObjectResponseModel<BookUniqueHomeDetailModel>> {
        const promise = this.http
            .post(ApiUrl.HOST_URI + 'SocialDetails', params);
        return new AsyncObjectPromiseHandler<BookUniqueHomeDetailModel>(promise.toPromise());
    }

    sendReviewAndRating(params): Promise<ObjectResponseModel<ReviewModal>> {
        const promise = this.http
            .post(ApiUrl.HOST_URI + 'Review', params);
        return new AsyncObjectPromiseHandler<ReviewModal>(promise.toPromise());
    }

    reportUniqueHomeListing(obj): Promise<ObjectResponseModel<ReviewModal>> {

        const promise = this.http
            .post(ApiUrl.HOST_URI + 'Review', obj);
        return new AsyncObjectPromiseHandler<ReviewModal>(promise.toPromise());
    }

}
