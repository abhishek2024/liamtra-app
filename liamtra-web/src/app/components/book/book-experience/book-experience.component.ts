import { Component, OnInit } from '@angular/core';
import { PaginationService } from '../../../shared/services';
import { BookService } from '../shared';
import { LoaderService } from '../../../core/loader/loader.service';
import { ApiUrl } from '../../../api.service';
import { ActivatedRoute, Router } from '@angular/router';
import { BookExpirenceService } from './shared/book-experience.service';
import { HostService } from '../../host/shared';
import { CdKeyEnum } from '../../../shared/enum/cd-key.enum';
@Component({
  selector: 'app-book-experience',
  templateUrl: './book-experience.component.html'
})

export class BookExperienceComponent implements OnInit {
  term: string;
  experiences: Array<any> = [];
  total = 0;
  page = 1;
  limit = 10;
  sliderCss: string = 'bookExperienceSearch';

  constructor(private paginationService: PaginationService,
    private loaderService: LoaderService,
    public router: Router,
    private bookService: BookService,
    public bookExperienceService: BookExpirenceService,
    public hostService: HostService) {

  }

  ngOnInit() {
    this.getServiceSubCategories();
  }

  async getBookIndividual() {
    try {
      this.loaderService.show();
      const response = await this.bookService.getBookIndividual(this.paginationService.getParams());
      console.log(response.data.Result);
      this.loaderService.hide();
    } catch (e) {
      console.log(e + 'here is the error');
      this.loaderService.hide();
    }
  }

  async getServiceSubCategories() {
    try {
      this.loaderService.show();
      const result = await this.hostService.getCdkeyById(CdKeyEnum.ExperienceType);
      this.experiences = result.data.Result;
      this.loaderService.hide();
    } catch (e) {
      console.log(e);
      this.loaderService.hide();
    }
  }

  goToPage(n: number): void {
    this.page = n;
    let obj = {
      page: this.page,
      rows: this.limit
    }
    this.paginationService.setPageChange(obj);
    this.getBookIndividual();
  }

  onNext(): void {
    this.page++;
    this.getBookIndividual();
  }

  onPrev(): void {
    this.page--;
    this.getBookIndividual();
  }

  routeToDetail(expId) {
    this.router.navigate(['book/experience-detail'], {
      queryParams: {
        SvcId: expId,
        Experience: '',
        CityName: ''
      }
    });
  }

  experienceList(data) {
    this.router.navigate(['/book/experience-list'], {
      queryParams: {
        SearchItem: data.id
      }
    });
  }

}
