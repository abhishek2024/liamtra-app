import { Component, OnInit } from '@angular/core';
import { PaginationService } from '../../../../shared/services';
import { BookService } from '../../shared';
import { LoaderService } from '../../../../core/loader/loader.service';
import { ApiUrl } from '../../../../api.service';
import { ActivatedRoute, Router } from '@angular/router';
import { BookExpirenceService } from '../shared/book-experience.service';
import { Subscription } from 'rxjs/Subscription';
import { BookExperienceModel, ExpFilterModel } from '../../book-tour';
import { AssertTypeEnum, CdKeyEnum } from '../../../../shared/enum/cd-key.enum';
import { ExperienceTypeModel, ExperienceDetailModel } from '../shared/book-experience.model';


@Component({
  selector: 'app-book-experience-list',
  templateUrl: './experience-list.component.html'
})

export class BookExperienceListComponent implements OnInit {

  experienceList: Array<BookExperienceModel> = Array<BookExperienceModel>();
  expirenceListType: Array<ExperienceTypeModel> = Array<ExperienceTypeModel>();
  experienceListDetail: ExperienceDetailModel = new ExperienceDetailModel();
  experienceFilterdata: ExpFilterModel = new ExpFilterModel();
  locations = [];
  filterTypeIds: string = '';
  sliderCss: string = 'bookExperienceSearch';
  locationCss: string = 'locationMapBooking';
  private subscription: Subscription;
  isMarkerDraggable: boolean = false;
  total: number = 0;
  page: number = 1;
  limit: number = 9;

  constructor(private paginationService: PaginationService,
    private loaderService: LoaderService,
    public router: Router,
    public route: ActivatedRoute,
    private bookService: BookService,
    public bookExperienceService: BookExpirenceService) { }

  ngOnInit() {
    this.getExperiencetype();
    this.route.queryParams.subscribe((param) => {
      this.experienceListDetail.ExperienceName = param['Experience'];
      this.experienceFilterdata.ExperienceName = param['Experience'];
      this.experienceListDetail.Name = param['SearchItem'];
      this.experienceFilterdata.Name = param['SearchItem'];
      this.experienceFilterdata.CityName = param['CityName'];
      this.experienceListDetail.CityName = param['CityName'];
      this.experienceListDetail.ExperienceType = param['ExperienceType'];
      this.getExperienceList();
    });
  }

  async getBookIndividual() {
    try {
      this.loaderService.show();
      const response = await this.bookService.getBookIndividual(this.paginationService.getParams());
      console.log(response.data.Result);
      this.loaderService.hide();
    } catch (e) {
      console.log(e + 'here is the error');
      this.loaderService.hide();
    }
  }

  async onFilterSearch(item) {

  }


  async getExperienceList() {
    try {
      this.loaderService.show();
      if (this.page === 1) {
        this.paginationConfig();
      }
      this.experienceListDetail.userId = +localStorage.getItem('userId') !== 0 ? +localStorage.getItem('userId') : 0;
      this.paginationService.pageSize = this.limit;
      this.paginationService.setFilterValues(this.experienceListDetail);
      const response = await this.bookExperienceService.getExperienceList(this.paginationService.getParams());
      this.experienceList = response.data.Result.data;
      this.total = response.data.Result.totalRecords;
      this.reArrangeData();
    } catch (e) {
      this.loaderService.hide();
    }
  }

  async getExperiencetype() {
    try {
      const response = await this.bookExperienceService.getExpirenceType(CdKeyEnum.ExperienceType);
      this.expirenceListType = response.data.Result;
    } catch (e) {
      console.log('error');
      // this.loaderService.hide();
    }
  }

  expirenceDetail(id) {
    this.router.navigate(['/book/experience-detail'], {
      queryParams: {
        CityName: this.experienceListDetail.CityName,
        id: id
      }
    });
  }

  async filteredExperience(data) {
    try {
      this.loaderService.show();
      data.isChecked = !data.isChecked;
      if (data.isChecked) {
        this.filterTypeIds = this.filterTypeIds !== '' ? this.filterTypeIds + ',' + data.id : data.id.toString();
      } else {
        const arr = this.filterTypeIds.split(',');
        const index = arr.findIndex(i => i === data.id.toString());
        arr.splice(index, 1);
        this.filterTypeIds = '';
        arr.forEach((id, i) => {
          if (i === 0) {
            this.filterTypeIds += id;
          } else {
            this.filterTypeIds += ',' + id;
          }
        });
      }
      this.experienceListDetail.ExperienceType = this.filterTypeIds;
      if (this.page === 1) {
        this.paginationConfig();
      }
      this.paginationService.setFilterValues(this.experienceListDetail);
      const response = await this.bookExperienceService.getExperienceFilteredList(this.paginationService.getParams());
      this.experienceList = response.data.Result.data;
      this.total = response.data.Result.totalRecords;
      this.reArrangeData();
    } catch (e) {
      this.loaderService.hide();
    }
  }


  reArrangeData() {
    this.locations = [];
    this.experienceList.forEach(experiencelisting => {
      this.locations.splice(this.locations.length, -1,
        { latitude: +(experiencelisting.latitude), longitude: +(experiencelisting.longitude) });
      experiencelisting.images = [];
      experiencelisting.videos = [];
      experiencelisting.userServiceAsset.forEach(asset => {
        if (asset.assetTypeId === AssertTypeEnum.Image) {
          const obj = {
            imgSrc: ApiUrl.SRC_URI + 'attachments/' + asset.fileName,
            sType: 'img'
          };
          experiencelisting.images.splice(experiencelisting.images.length, 0, obj);
        } else if (asset.assetTypeId === AssertTypeEnum.Video) {
          const obj = {
            videoSrc: ApiUrl.SRC_URI + asset.attachmentUrl
          };
          experiencelisting.videos.splice(experiencelisting.videos.length, 0, obj);
        }
      });
    });
    if (this.locations.length === 0) {
      this.locations = [{ 'latitude': 28.89813800000001, 'longitude': 76.56141400000001 }];
    }

    this.loaderService.hide();
  }

  goToPage(n: number): void {
    this.page = n;
    const obj = {
      page: this.page,
      rows: this.limit
    };
    this.paginationService.setPageChange(obj);
    this.getExperienceList();
  }

  onNext(): void {
    this.page++;
    this.paginationConfig();
    this.getExperienceList();
  }

  onPrev(): void {
    this.page--;
    this.paginationConfig();
    this.getExperienceList();
  }

  paginationConfig() {
    const obj = {
      page: this.page,
      rows: this.limit
    };
    this.paginationService.setPageChange(obj);
  }
}
