import { Component, OnInit, EventEmitter, Output, Pipe, PipeTransform, Input } from '@angular/core';
import { BookService, ServiceSubCategoryModel } from '../../shared';
import { LoaderService } from '../../../../core/loader/loader.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import { ApiUrl } from '../../../../api.service';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageService } from '../../../../shared/message/messageService.service';

@Component({
    selector: 'app-book-experience-search-filter',
    templateUrl: './search-filter.component.html'
})

export class BookExperienceSearchFilterComponent implements OnInit {
    @Input() searchItem: any;
    @Input() isExpMain: any;
    @Input() isExpList: any;
    @Input() name: string;
    @Input() cityName: string;
    city: string = '';
    experience: string = '';

    constructor(public http: Http,
        private loaderService: LoaderService,
        private bookService: BookService,
        public router: Router,
        private messageService: MessageService) {
    }


    ngOnInit() {
        this.experience = this.name;
        this.city = this.cityName;
    }

    async getBookExperience() {
        if (this.experience || this.city) {
            this.router.navigate(['book/experience-list'], {
                queryParams: {
                    Experience: this.experience,
                    CityName: this.city,
                    SearchItem: this.searchItem
                }
            });
        } else {
            this.messageService.showMessage({ type: 'error', title: 'Experience', body: 'Please Select Experience Name Or City' });

        }

    }



    observableSource = (keyword: any): Observable<any[]> => {
        const url = ApiUrl.MASTER_URI + 'HostExperience/GetByExperienceName/' + keyword;
        if (keyword) {
            return this.http.get(url)
                .map(res => {
                    const json = res.json();
                    this.loaderService.hide();
                    return json;
                });
        } else {
            return Observable.of([]);
        }
    }

    cityObservableSource = (keyword: any): Observable<any[]> => {
        const url = ApiUrl.MASTER_URI + 'City/listForDestinationSearch/' + keyword;
        if (keyword) {
            return this.http.get(url)
                .map(res => {
                    const json = res.json();
                    this.loaderService.hide();
                    return json;
                });
        } else {
            return Observable.of([]);
        }
    }
}
