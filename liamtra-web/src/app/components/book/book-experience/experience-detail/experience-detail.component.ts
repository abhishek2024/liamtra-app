import { Component, OnInit, ViewChild } from '@angular/core';
import { BookService, FavoriteModel, ReportModel, LanguageModel } from '../../shared';
import { LoaderService } from '../../../../core/loader/loader.service';
import { ActivatedRoute, Router } from '@angular/router';
import { BookExpirenceService } from '../shared/book-experience.service';
import { BookExperienceModel,  } from '../../book-tour';
import { ApiUrl } from '../../../../api.service';
import { AssertTypeEnum, UserServiceReportEnum } from '../../../../shared/enum/cd-key.enum';
import { MessageService } from '../../../../shared/message/messageService.service';
import { CdKeyEnum } from '../../../../shared/enum/cd-key.enum';
import { PaginationService, LocalStorageService } from '../../../../shared/services';
import { BookUniqueHomeReportListing } from '../../../../core/dialog-component/book-unique-home-report/book-unique-home-report.component'
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ExperienceDetailModel } from "../shared/book-experience.model";

@Component({
    selector: 'app-book-experience-detail',
    templateUrl: './experience-detail.component.html'
})

export class BookExperienceDetailComponent implements OnInit {
    experienceDetail: BookExperienceModel = new BookExperienceModel();
    experienceListDetail: ExperienceDetailModel = new ExperienceDetailModel();
    locationCss: string = 'bookTourMap';
    favoriteModel: FavoriteModel = new FavoriteModel();
    reportListing: ReportModel = new ReportModel();
    modal: boolean = false;
    modalStatus: string = '';
    languages: Array<LanguageModel> = [];
    modalType: number;
    sliderCss: string = 'bookUniqueHomeDetail';
    reviewModal: boolean = false;
    isMarkerDraggable: boolean = false;
    @ViewChild('profile') profile;
    @ViewChild('reviews') reviews;

    constructor(private loaderService: LoaderService,
        private router: Router,
        public route: ActivatedRoute,
        private localStorageService: LocalStorageService,
        private messageService: MessageService,
        public dialog: MatDialog,
        private bookService: BookService,
        private bookExperienceService: BookExpirenceService,
        private paginationService: PaginationService) {

    }

    ngOnInit() {
        this.route.queryParams.subscribe((param) => {
            this.experienceListDetail.id = +param['id'];
            this.experienceDetail.cityName = param['CityName'];
            this.getBookExperienceIndividual();
        });
    }

    async getBookExperienceIndividual() {
        try {
            this.loaderService.show();
            this.experienceListDetail.userId = +localStorage.getItem('userId') !== 0 ? +localStorage.getItem('userId') : 0;
            this.paginationService.setFilterValues(this.experienceListDetail);
            const response = await this.bookExperienceService.getExperienceIndividual(this.experienceListDetail.id,
                this.experienceListDetail.userId);
            this.experienceDetail = response.data.Result;
            this.experienceDetail.userServiceMultiValues.forEach(item => {
                if (item.cdKeyId === CdKeyEnum.Languages) {
                    this.languages.push(item);
                }
            });
            this.experienceDetail.images = [];
            this.experienceDetail.videos = [];
            this.experienceDetail.userServiceAsset.forEach(asset => {
                if (asset.assetTypeId === AssertTypeEnum.Image) {
                    const obj = {
                        imgSrc: ApiUrl.SRC_URI + 'attachments/' + asset.fileName,
                        sType: 'img'
                    };
                    this.experienceDetail.images.splice(this.experienceDetail.images.length, 0, obj);
                } else if (asset.assetTypeId === AssertTypeEnum.Video) {
                    const obj = {
                        videoSrc: ApiUrl.SRC_URI + asset.attachmentUrl
                    };
                    this.experienceDetail.videos.splice(this.experienceDetail.videos.length, 0, obj);
                }
            });
            this.loaderService.hide();
        } catch (e) {
            this.loaderService.hide();
        }

    }

    modalPopupEvent(result) {
        this.modal = result;
    }

    opemModal(modalStatus) {
        this.modalStatus = modalStatus;
        this.modal = true;
    }

    async checkUserLogin() {
        try {
            const access_token = this.localStorageService.getAccessToken();
            if (access_token) {
                return true;
            } else {
                this.opemModal('signIn');
                return false;
            }
        } catch (e) { }
    }

    async openDialog() {
        const isLogin: boolean = await this.checkUserLogin();
        if (isLogin) {
            const dialogRef = this.dialog.open(BookUniqueHomeReportListing, {
                width: '1100px',
                data: { name: 'Experience Report Modal' }
            });
            dialogRef.afterClosed().subscribe(async result => {
                if (result) {
                    this.reportListing.userServiceId = this.experienceDetail.userServiceId;
                    this.reportListing.remarks = result;
                    this.reportListing.cdKeyValueId = UserServiceReportEnum.Requested;
                    try {
                        this.loaderService.show();
                        const response = await this.bookService.reportIssue(this.reportListing);
                        if (response.data.Result) {
                            this.messageService.showMessage({ type: 'success', title: '', body: 'Reported Successfully' });
                        }
                        this.loaderService.hide();
                    } catch (e) {
                        this.loaderService.hide();
                    }
                }
            });
        }
    }

    async bookFavorite() {
        try {
            const access_token = this.localStorageService.getAccessToken();
            if (access_token) {
                this.favoriteModel.isFavorite = !this.favoriteModel.isFavorite;
                this.favoriteModel.userServiceId = this.experienceDetail.userServiceId;
                this.loaderService.show();
                const response = await this.bookService.bookFavorite(this.favoriteModel);
                this.getBookExperienceIndividual();
            } else {
                this.modalStatus = 'signIn';
                this.modal = true;
                this.messageService.showMessage({ type: 'error', title: 'Business', body: 'Please login before make it favorite' });
            }
        } catch (e) {
            this.loaderService.hide();
        }
    }

    closePopupEvent(result) {
        this.reviewModal = result;
        this.getBookExperienceIndividual();
    }

    openReviewModal() {
        const access_token = this.localStorageService.getAccessToken();
        if (access_token) {
            this.reviewModal = true;
        } else {
            this.modalStatus = 'signIn';
            this.modal = true;
            this.messageService.showMessage({ type: 'error', title: 'Business', body: 'Please login before review it' });
        }
    }


    scroll(el) {
        if (el === 'reviews') {
            this.reviews.nativeElement.scrollIntoView({ behavior: "smooth" });
        }
        if (el === 'profile') {
            this.profile.nativeElement.scrollIntoView({ behavior: "smooth" });
        }
    }
}
