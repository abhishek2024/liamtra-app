export class ExperienceTypeModel {
  cdKeyId: number;
  createdBy: number;
  creationDate: string;
  displayName: string;
  displayOrder: number;
  id: number;
  isActive: boolean;
  modificationDate: string;
  modifiedBy: number;
  value: number;
  isChecked?: boolean = false;
}

export class ExperienceDetailModel {
  id: number = 0;
  userId: number = 0;
  ExperienceName: string;
  CityName: string;
  Name: string;
  ExperienceType: string;
}
