import { Http, Headers, RequestOptions, BaseRequestOptions, Response } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { ApiUrl } from '../../../../api.service';
import {
    ObjectResponseModel,
    PostObjectResponseModel,
    ArrayResponseModel,
    AsyncArrayPromiseHandler,
    AsyncObjectPromiseHandler,
    BaseDataModel
} from '../../../../shared/models/base-data.model';
import { CityModel } from '../../../shared/city.model';
import { BookExperienceModel } from '../../book-tour';
@Injectable()
export class BookExpirenceService {

    constructor(private http: Http) { }

    // getExperienceList(params): Promise<ArrayResponseModel<any>> {
    //     const promise = this.http.get(ApiUrl.HOST_URI + 'HostExperience/GetListing', { params: params });
    //     return new AsyncArrayPromiseHandler<any>(promise.toPromise());
    // }

    getExperienceList(params): Promise<ObjectResponseModel<BaseDataModel<any>>> {
        const promise = this.http.get(ApiUrl.HOST_URI + 'HostExperience/GetListing', { params: params });
        return new AsyncObjectPromiseHandler<BaseDataModel<any>>(promise.toPromise());
    }

    getExperienceIndividual(id, userId): Promise<ObjectResponseModel<any>> {
        const promise = this.http.get(ApiUrl.HOST_URI + 'HostExperience/GetById/' + id + '/' + userId);
        return new AsyncObjectPromiseHandler<any>(promise.toPromise());
    }


    getExp() {
        return this.http.get('assets/exp.json')
            .map((res: any) => res.json());
    }

    // getExpirenceType(id): Promise<ArrayResponseModel<BaseDataModel<any>>> {
    //     const promise = this.http.get(ApiUrl.HOST_URI + 'CdKeyValue/GetByCDKeyID/' + id);
    //     return new AsyncArrayPromiseHandler<BaseDataModel<any>>(promise.toPromise());
    // }

    getExpirenceType(id): Promise<ArrayResponseModel<any>> {
        const promise = this.http.get(ApiUrl.HOST_URI + 'CdKeyValue/GetByCDKeyID/' + id);
        return new AsyncArrayPromiseHandler<any>(promise.toPromise());
    }

    getExperienceDetail(params): Promise<ArrayResponseModel<any>> {
        const promise = this.http.get(ApiUrl.HOST_URI + 'CdKeyValue/GetByCDKeyID/');
        return new AsyncArrayPromiseHandler<any>(promise.toPromise());
    }

    getExperienceFilteredList(params): Promise<ObjectResponseModel<BaseDataModel<any>>> {
        const promise = this.http.get(ApiUrl.HOST_URI + 'HostExperience/GetListing', { params: params });
        return new AsyncObjectPromiseHandler<BaseDataModel<any>>(promise.toPromise());
    }
}
