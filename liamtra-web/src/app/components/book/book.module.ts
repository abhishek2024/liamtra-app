
import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { BookExperienceComponent, BookExperienceDetailComponent } from './book-experience';
import { BookFlightComponent } from './book-flight/book-flight.component';
import { BookHotalComponent } from './book-hotal/book-hotal.component';
import {
  BookTaxiComponent,
  BookTaxiSearchComponent,
  BookTaxiDetailComponent,
  BookTaxiFilterComponent,
  BookTaxiService
} from './book-taxi/index';
import { BookUberComponent } from './book-uber/book-uber.component';
import { CoreService } from '../../core/shared';
import {
  BookUniqueHomeDetailComponent,
  BookUniqueHomeComponent,
  BookUniqueHomeSearchComponent,
  BookUniqueHomeService,
  BookHomeCheckoutComponent,
  BookGuestIdentityComponent,
  BookGuestEntryComponent,
  BookGuestBookDetailComponent,
  BookHomeCheckoutService,
  VideoModalComponent
} from './book-unique-home/index';


import {
  BookUniqueSearchFilterComponent
} from './book-unique-search-filter/book-unique-search-filter.component';
import { BookRouting } from './book.routing';
import { BookService } from '../book/shared';
import { BookThankYouComponent } from './shared/thank-you/thank-you.component';
import { RatingModule } from 'ng2-rating';
import { BookTourService } from './book-tour/index';
import {
  BookTourSearchFilterComponent,
  // BookTourCheckoutComponent,
  BookTourDetailComponent,
  BookTourComponent,
  BookTourListingComponent
} from './book-tour';

import { BookTourCheckoutComponent } from './book-tour/tour-checkout/tour-checkout.component';
import { BookExperienceSearchFilterComponent } from './book-experience/search-filter/search-filter.component';
import { BookExpirenceService } from './book-experience/shared/book-experience.service';
import { BookBusinessComponent } from './book-bussiness/book-business.component';
import { BookBussinessSearchComponent } from './book-bussiness/business-search/bussiness-search.component';
import { BookBusinessService } from './book-bussiness/shared/book-business.service';
import { BookBusinessListComponent } from './book-bussiness/business-list/business-list.component';
import { BookBusinessDetailComponent } from './book-bussiness/business-detail/business-detail.component';
import { BookExperienceListComponent } from './book-experience/experience-list/experience-list.component';
import { HostService } from '../host/shared';
import { ReviewModalComponent } from './shared/review-modal/review-modal.component';


// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
@NgModule({

  declarations: [
    BookExperienceSearchFilterComponent,
    BookExperienceComponent,
    BookExperienceDetailComponent,
    BookFlightComponent,
    BookHotalComponent,
    BookTaxiComponent,
    BookTourComponent,
    BookUberComponent,
    BookUniqueHomeComponent,
    BookUniqueHomeSearchComponent,
    BookBusinessComponent,
    BookUniqueSearchFilterComponent,
    BookUniqueHomeDetailComponent,
    BookHomeCheckoutComponent,
    BookGuestIdentityComponent,
    BookGuestEntryComponent,
    BookGuestBookDetailComponent,
    BookThankYouComponent,
    VideoModalComponent,
    BookTaxiSearchComponent,
    BookTaxiDetailComponent,
    BookTourSearchFilterComponent,
    BookTourListingComponent,
    BookBussinessSearchComponent,
    BookTaxiFilterComponent,
    BookTourDetailComponent,
    BookTourCheckoutComponent,
    BookBusinessListComponent,
    BookBusinessDetailComponent,
    BookExperienceListComponent,
    ReviewModalComponent
  ],
  imports: [
    SharedModule,
    BookRouting,
    RatingModule,
  ],
  providers: [
    BookService,
    BookUniqueHomeService,
    BookHomeCheckoutService,
    CoreService,
    BookTourService,
    BookTaxiService,
    BookTourService,
    BookExpirenceService,
    BookBusinessService,
    HostService
  ],
  exports: [
    SharedModule
  ]
})

export class BookModule { }
