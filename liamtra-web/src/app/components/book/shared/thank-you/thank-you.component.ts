import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
@Component({
  selector: 'app-book-thank-you',
  templateUrl: './thank-you.component.html'
})
export class BookThankYouComponent implements OnInit {

  transactionId: string = "";
  constructor(private router: Router,
    public route: ActivatedRoute) {
  }

  ngOnInit() {
    this.transactionId = this.route.snapshot.params.genValue;
  }

}