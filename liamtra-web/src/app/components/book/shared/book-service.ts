import { Http, Headers, RequestOptions, BaseRequestOptions, Response } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { ApiUrl } from '../../../api.service';
import { FilterBookUniqueHomeModel } from '../book-unique-home/shared/book-unique-home.model'
import {
    ObjectResponseModel,
    PostObjectResponseModel,
    ArrayResponseModel,
    AsyncArrayPromiseHandler,
    AsyncObjectPromiseHandler,
    BaseDataModel
} from '../../../shared/models/base-data.model';
import { BookUnique, BookUniqueHomeSearchData, ServiceSubCategoryModel } from './book-model';
import { CityModel } from '../../shared/city.model';
import { BookExperienceModel } from '../book-tour';
@Injectable()
export class BookService {

    constructor(private http: Http) { }

    getBookIndividual(params): Promise<ObjectResponseModel<BaseDataModel<BookUniqueHomeSearchData>>> {
        const promise = this.http
            .get(ApiUrl.BOOK_URI + 'BookService/GetUniqueHomeListing/', { params: params });
        return new AsyncObjectPromiseHandler<BaseDataModel<BookUniqueHomeSearchData>>(promise.toPromise());
    }
    getServiceSubCategoryByServiceId(serviceCategoryId): Promise<ArrayResponseModel<ServiceSubCategoryModel>> {
        const promise = this.http
            .get(ApiUrl.BOOK_URI + 'ServiceSubCategory/list/' + serviceCategoryId);
        return new AsyncArrayPromiseHandler<ServiceSubCategoryModel>(promise.toPromise());

    }

    getCityListBySearchString(cityName): Promise<ArrayResponseModel<CityModel>> {
        const promise = this.http
            .get(ApiUrl.BOOK_URI + 'City/listForDestinationSearch/' + cityName);
        return new AsyncArrayPromiseHandler<CityModel>(promise.toPromise());
    }

    // GET /api/BookService/GetUniqueHomeListingByFilter
    getUniqueHomeListing(params): Promise<ObjectResponseModel<BaseDataModel<BookUniqueHomeSearchData>>> {
        const promise = this.http
            .get(ApiUrl.BOOK_URI + 'BookService/GetUniqueHomeListing/', { params: params });
        return new AsyncObjectPromiseHandler<BaseDataModel<BookUniqueHomeSearchData>>(promise.toPromise());
    }

    getUniqueHomeListingWithFilter(params): Promise<ObjectResponseModel<BaseDataModel<BookUniqueHomeSearchData>>> {
        const promise = this.http
            .get(ApiUrl.BOOK_URI + 'BookService/GetUniqueHomeListingByFilter/', { params: params });
        return new AsyncObjectPromiseHandler<BaseDataModel<BookUniqueHomeSearchData>>(promise.toPromise());
    }

    geExperiencetWithFilters(params): Promise<ObjectResponseModel<BaseDataModel<BookUniqueHomeSearchData>>> {
        const promise = this.http
            .get(ApiUrl.BOOK_URI + 'BookService/GetExperienceListingByFilters/', { params: params });
        return new AsyncObjectPromiseHandler<BaseDataModel<BookUniqueHomeSearchData>>(promise.toPromise());
    }

    bookFavorite(data): Promise<ObjectResponseModel<any>> {
        let promise = this.http.post(ApiUrl.BOOK_URI + 'UserServiceFavorite', data);
        return new AsyncObjectPromiseHandler<any>(promise.toPromise());
    }

    bookFavoriteUniqueHome(data): Promise<ObjectResponseModel<any>> {
        let promise = this.http.put(ApiUrl.BOOK_URI + 'HostServiceLinkingSubCategory/updateForFavorite', data);
        return new AsyncObjectPromiseHandler<any>(promise.toPromise());
    }

    reportIssue(data): Promise<ObjectResponseModel<any>> {
        let promise = this.http.post(ApiUrl.BOOK_URI + 'UserServiceReport', data);
        return new AsyncObjectPromiseHandler<any>(promise.toPromise());
    }

    sendReviewAndRating(data): Promise<ObjectResponseModel<any>> {
        let promise = this.http.post(ApiUrl.BOOK_URI + 'UserServiceReview', data);
        return new AsyncObjectPromiseHandler<any>(promise.toPromise());
    }

}
