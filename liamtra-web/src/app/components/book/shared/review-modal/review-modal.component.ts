import { Router } from '@angular/router';
import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ElementRef
} from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { LoaderService } from '../../../../core/loader';
import { LoginEnum } from '../../../../shared/enum/login.enum';
import { BookService } from '../book-service';
import { MessageService } from '../../../../shared/message/messageService.service';


@Component({
  selector: 'app-review-modal',
  templateUrl: './review-modal.component.html'
})

export class ReviewModalComponent implements OnInit {
  private subscription: Subscription;
  count: number = 0;
  @Output() closePopupEvent = new EventEmitter();
  @Input() modal: boolean = true;
  @Input() userServiceId: number = 0;
  ratingValue: number = 0;
  remarks: string = '';

  constructor(private eRef: ElementRef,
    private bookService: BookService,
    private messageService: MessageService,
    private loaderService: LoaderService) {
  }

  ngOnInit() { }

  closeModal() {
    this.modal = false;
    this.closePopupEvent.emit(this.modal);
  }

  async  sendReviewAndRating() {
    try {

      if (this.ratingValue === 0) {
        this.messageService.showMessage({ type: 'error', title: 'Business', body: 'Rating is mandatory' });
        return;
      }
      let obj = {
        ratingValue: this.ratingValue,
        remarks: this.remarks,
        userServiceId: this.userServiceId
      }
      this.loaderService.show();
      let response = await this.bookService.sendReviewAndRating(obj);
      this.loaderService.hide();
      this.modal = false;
      this.closePopupEvent.emit(this.modal);
    } catch (e) {
      this.loaderService.hide();
    }
  }
}
