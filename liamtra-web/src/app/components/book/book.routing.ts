import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { BookExperienceComponent, BookExperienceDetailComponent } from './book-experience';
import { BookFlightComponent } from './book-flight/book-flight.component';
import { BookHotalComponent } from './book-hotal/book-hotal.component';
import {
  BookTaxiComponent,
  BookTaxiSearchComponent,
  BookTaxiDetailComponent
} from './book-taxi/index';
import { BookUberComponent } from './book-uber/book-uber.component';
import {
  BookTourSearchFilterComponent,
  BookTourCheckoutComponent,
  BookTourDetailComponent,
  BookTourComponent,
  BookTourListingComponent
} from './book-tour';
import {
  BookUniqueHomeComponent,
  BookUniqueHomeDetailComponent,
  BookUniqueHomeSearchComponent,
  BookHomeCheckoutComponent
} from './book-unique-home/index';
import { BookThankYouComponent } from './shared/thank-you/thank-you.component';
import { BookBusinessComponent } from './book-bussiness/book-business.component';
import { BookBusinessListComponent } from './book-bussiness/business-list/business-list.component';
import { BookBusinessDetailComponent } from './book-bussiness/business-detail/business-detail.component';
import { BookExperienceListComponent } from './book-experience/experience-list/experience-list.component';
import { AuthGuard } from '../../shared/guard';

export const routes: Routes = [
  {
    path: '',
    children: [
      { path: 'experience', component: BookExperienceComponent },
      { path: 'experience-detail', component: BookExperienceDetailComponent },
      { path: 'flight', component: BookFlightComponent },
      { path: 'hotel', component: BookHotalComponent },
      { path: 'car', component: BookTaxiComponent },
      { path: 'car-search', component: BookTaxiSearchComponent },
      { path: 'car-detail', component: BookTaxiDetailComponent },
      { path: 'car', component: BookTaxiComponent },
      { path: 'tour', component: BookTourComponent },
      { path: 'uber', component: BookUberComponent },
      { path: 'unique-homes', component: BookUniqueHomeComponent },
      { path: 'unique-homes-search', component: BookUniqueHomeSearchComponent },
      { path: 'unique-homes-detail', component: BookUniqueHomeDetailComponent },
      { path: 'vendor', component: BookBusinessComponent },
      { path: 'checkout', component: BookHomeCheckoutComponent },
      { path: 'thank-you/:genValue', component: BookThankYouComponent },
      { path: 'tour-listing', component: BookTourListingComponent },
      { path: 'tour-detail/:tourId', component: BookTourDetailComponent },
      { path: 'tour-checkout', component: BookTourCheckoutComponent },
      { path: 'business-list', component: BookBusinessListComponent },
      { path: 'business-detail', component: BookBusinessDetailComponent },
      { path: 'experience-list', component: BookExperienceListComponent },

    ]
  }
];

export const BookRouting: ModuleWithProviders = RouterModule.forChild(routes);
