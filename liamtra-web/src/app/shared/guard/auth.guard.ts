import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { LocalStorageService } from '../../shared/services/local-storage.service';

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(private router: Router,
        private localStorageService: LocalStorageService) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const currentUser = this.localStorageService.getCurrentUser();
        if (currentUser) {
            // logged in so return true
            // const returnUrl = state.url.split('/')[1];
            // if (returnUrl) {
            //     // this.localStorageService.setModuleName(returnUrl);
            // }
            return true;
        }

        // not logged in so redirect to login page with the return url
        this.router.navigate(['/login'], { queryParams: { returnUrl: state.url } });
        return false;
    }
}
