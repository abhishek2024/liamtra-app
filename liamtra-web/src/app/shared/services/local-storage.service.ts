import { Injectable } from '@angular/core';

@Injectable()
export class LocalStorageService {

  constructor() {
  }

  public getCurrentUser() {
    if (localStorage.getItem('currentUser')) {
      return JSON.parse(localStorage.getItem('currentUser'));
    } else {
      return null;
    }
  }

  public getUserDetail() {
    const user = this.getCurrentUser();
    if (user) {
      return user.User;
    }
    return null;
  }

  public getAccessToken(): string {
    const currentUser = this.getCurrentUser();
    if (currentUser) {
      localStorage.setItem('userId', currentUser.User.UserId);
      return 'Bearer ' + currentUser.AccessToken;
    }
    return '';
  }

  public getLoggedUser() {
    return JSON.parse(localStorage.getItem('loggedUser'));
  }

  public setCurrentUser(token) {
    localStorage.removeItem('currentUser');
    localStorage.setItem('currentUser', JSON.stringify(token));
  }

  public setGlobalInformation(info) {
    localStorage.removeItem('globalInfo');
    localStorage.setItem('globalInfo', JSON.stringify(info));
  }

  public getGlobalInformation() {
    if (localStorage.getItem('globalInfo')) {
      return JSON.parse(localStorage.getItem('globalInfo'));
    } else {
      return null;
    }
  }

  public removeLogin() {
    localStorage.removeItem('currentUser');
    localStorage.removeItem('userId');
  }
}
