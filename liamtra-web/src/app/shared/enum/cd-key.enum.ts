export enum CdKeyEnum {
  Languages = 1,
  RoomType = 2,
  PropertyType = 3,
  TotalRooms = 4,
  TotalBeds = 5,
  TotalBathrooms = 6,
  TotalAdultGuest = 7,
  TotalChildGuest = 8,
  Amenities = 9,
  Highlights = 10,
  PlaceType = 11,
  BookingPreferences = 12,
  ExperienceType = 13,
  BusinessType = 14
}

export enum AssertTypeEnum {
  Image = 1,
  Video = 2,
  Pdf = 3
}

export enum UserServiceReportEnum {
  Requested = 121,
  Verified = 122,
  Rejected = 123,
}
